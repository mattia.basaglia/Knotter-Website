<?php

/**
 * This is the model class for table "ticket_priority".
 *
 * The followings are the available columns in table 'ticket_priority':
 * @property integer $id_ticket_priority
 * @property string $name
 * @property integer $weight
 *
 * The followings are the available model relations:
 * @property Ticket[] $tickets
 */
class TicketPriority extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TicketPriority the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    static function findByName($name)
    {
        return self::model()->findByAttributes(
                                array('name'=>$name));
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ticket_priority';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, weight', 'required'),
			array('name', 'length', 'max'=>64),
            array('weight','numeric','integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_ticket_priority, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tickets' => array(self::HAS_MANY, 'Ticket', 'id_priority'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ticket_priority' => 'Id Ticket Priority',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ticket_priority',$this->id_ticket_priority);
		$criteria->compare('name',$this->name,true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}