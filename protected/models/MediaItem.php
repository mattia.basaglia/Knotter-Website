<?php

/**
 * This is the model class for table "media_item".
 *
 * The followings are the available columns in table 'media_item':
 * @property integer $id_media_item
 * @property string $file
 * @property string $mime_type
 * @property string $title
 * @property string $description
 * @property string $thumb
 *
 * The followings are the available model relations:
 * @property Gallery[] $galleries
 * @property GalleryItem[] $gallery_positions
 */
class MediaItem extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MediaItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'media_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('file, mime_type, title, description', 'required'),
			array('file, title, thumb', 'length', 'max'=>64),
			array('mime_type', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_media_item, file, mime_type, title, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'galleries' => array(self::MANY_MANY, 'Gallery', 'gallery_item(id_media_item, id_gallery)'),
            'gallery_positions' =>
                array(self::HAS_MANY,'GalleryItem','id_media_item')
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_media_item' => 'Id Media Item',
			'file' => 'File',
			'mime_type' => 'Mime Type',
			'title' => 'Title',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_media_item',$this->id_media_item);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('mime_type',$this->mime_type,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
    
    function desc_url()
    {
        return array('media/view','id'=>$this->id_media_item);
    }
    
    function resource_url()
    {
        return Controller::base_url().'/img/'.$this->file;
    }
    
    function thumb_url()
    {
        return Controller::base_url().'/img/'.
            ($this->thumb==null?$this->file:$this->thumb);
    }
    
    function render_thumb($htmlOptions=array())
    {
        return CHtml::image( $this->thumb_url(), $this->title, $htmlOptions );
    }
    
    function render($htmlOptions=array())
    {
        
        $htmlOptions['title'] = $this->title;
        if ( preg_match('#image/.+#',$this->mime_type) )
        {
            return CHtml::image( $this->resource_url(), $this->title, $htmlOptions );
        }
        else
            throw new CHttpException(500, "Unknown media type $this->mime_type".
                            "detected for media item $this->id_media_item");
    }
    
    function render_link($target=null,$imageOptions=array())
    {
        if ( $target == null )
            $target = $this->resource_url();
        return CHtml::link($this->render_thumb($imageOptions),$target);
    }
    
    // returns a string that should help identify this item
    function id_string()
    {
        return $this->title." - ".$this->file;
    }
}