<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id_user
 * @property string $name
 * @property string $description
 * @property integer $salt
 * @property string $password
 * @property integer $id_role
 * @property string $email
 *
 * The followings are the available model relations:
 * @property Comment[] $comments
 * @property Edit[] $edits
 * @property Ticket[] $tickets
 * @property UserRole $role
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, salt, password, id_role, email', 'required'),
			array('salt, id_role', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>32),
			array('description', 'length', 'max'=>45),
			array('password', 'length', 'max'=>20),
			array('email', 'length', 'max'=>128),
            array('email','email'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_user, name, description, salt, password, id_role, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'comments' => array(self::HAS_MANY, 'Comment', 'id_author'),
			'edits' => array(self::HAS_MANY, 'Edit', 'id_user'),
			'tickets' => array(self::HAS_MANY, 'Ticket', 'id_assignee'),
			'role' => array(self::BELONGS_TO, 'UserRole', 'id_role'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_user' => 'Id User',
			'name' => 'Name',
			'description' => 'Description',
			'salt' => 'Salt',
			'password' => 'Password',
			'id_role' => 'Id Role',
			'email' => 'Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('salt',$this->salt);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('id_role',$this->id_role);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
    function valid_password ( $pass )
    {
        return sha1($pass.$this->salt) == $this->password;
    }
    
    function can_login()
    {
        return $this->id_role != 4;
    }
    
    function avatar_url($size=32)
    {
        return "http://www.gravatar.com/avatar/" .
            md5( strtolower( trim( $this->email ) ) ) .
            "?s=" . $size;
    }
    
    function avatar_image($size=32,$htmlOptions=array())
    {
        $htmlOptions['width'] = $size;
        $htmlOptions['height'] = $size;
        return CHtml::image($this->avatar_url(),''/*$this->name*/,$htmlOptions);
    }
    
    function url()
    {
        return array('user/view','name'=>$this->name);
    }
    
    function link()
    {
        return CHtml::link($this->name,$this->url());
    }
    
    
}