<?php

/**
 * This is the model class for table "edit".
 *
 * The followings are the available columns in table 'edit':
 * @property integer $id_edit
 * @property string $date
 * @property string $text
 * @property integer $id_user
 * @property integer $id_post
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Post $post
 */
class Edit extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Edit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'edit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('text', 'required', 'except'=>'autofill'),
			array('text', 'length', 'min'=>3 ),
            array('date, id_post, id_user','safe'),//handled in controller
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_edit, date, text, id_user, id_post', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'id_user'),
			'post' => array(self::BELONGS_TO, 'Post', 'id_post'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_edit' => 'Id Edit',
			'date' => 'Date',
			'text' => 'Text',
			'id_user' => 'User',
			'id_post' => 'Post',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_edit',$this->id_edit);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_post',$this->id_post);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}