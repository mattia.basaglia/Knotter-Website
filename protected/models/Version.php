<?php

/**
 * This is the model class for table "version".
 *
 * The followings are the available columns in table 'version':
 * @property integer $id_version
 * @property string $name
 * @property integer $id_project
 * @property integer $id_release_notes
 *
 * The followings are the available model relations:
 * @property Download[] $downloads
 * @property Ticket[] $tickets
 * @property Project $project
 * @property Post $releaseNotes
 */
class Version extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Version the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'version';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, id_project', 'required'),
			array('id_project, id_release_notes', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_version, name, id_project, id_release_notes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'downloads' => array(self::HAS_MANY, 'Download', 'id_version'),
			'tickets' => array(self::HAS_MANY, 'Ticket', 'id_version'),
			'project' => array(self::BELONGS_TO, 'Project', 'id_project'),
			'releaseNotes' => array(self::BELONGS_TO, 'Post', 'id_release_notes'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_version' => 'Id Version',
			'name' => 'Name',
			'id_project' => 'Id Project',
			'id_release_notes' => 'Id Release Notes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_version',$this->id_version);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('id_project',$this->id_project);
		$criteria->compare('id_release_notes',$this->id_release_notes);
        $criteria->order ='name desc';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}