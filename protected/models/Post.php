<?php

/**
 * This is the model class for table "post".
 *
 * The followings are the available columns in table 'post':
 * @property integer $id_post
 * @property string $title
 * @property integer $locked
 * @property integer $id_type
 * @property string $page
 * @property integer $id_gallery
 * @property integer $id_parent
 * @property integer $visible
 *
 * The followings are the available model relations:
 * @property Edit[] $edits
 * @property Edit $content Current text
 * @property Edit $source First version
 * @property Post $parent
 * @property Post[] $children
 * @property Gallery $gallery
 * @property PostType $type
 * @property Ticket $ticket
 * @property Version $version
 */
class Post extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    static function from_page($name,$type)
    {
        $type = PostType::findByName($type);
        return self::model()->findByAttributes(
                        array('page'=>$name, 'id_type'=>$type->id_post_type)
        );
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'post';
	}
    
    function filter_title($text)
    {
        return str_replace('|','-',trim($text));
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, id_type', 'required'),
			array('visible, locked, id_type, id_gallery, id_parent', 'numerical', 'integerOnly'=>true),
			array('title, page', 'length', 'max'=>255),
            array('title','filter','filter'=>array($this,'filter_title')),
            array('title', 'length', 'min'=>3),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_post, title, locked, id_type, page, id_gallery, id_parent', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
    		'source' => array(self::HAS_ONE, 'Edit', 'id_post', 'order'=>'date' ),	
            'content' => array(self::HAS_ONE, 'Edit', 'id_post', 'order'=>'date desc'),
			'edits' => array(self::HAS_MANY, 'Edit', 'id_post'),
			'parent' => array(self::BELONGS_TO, 'Post', 'id_parent'),
			'children' => array(self::HAS_MANY, 'Post', 'id_parent'),
			'gallery' => array(self::BELONGS_TO, 'Gallery', 'id_gallery'),
			'type' => array(self::BELONGS_TO, 'PostType', 'id_type'),
			'ticket' => array(self::HAS_ONE, 'Ticket', 'id_post'),
			'version' => array(self::HAS_ONE, 'Version', 'id_release_notes'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_post' => 'Post',
			'title' => 'Title',
			'locked' => 'Locked',
			'id_type' => 'Type',
			'page' => 'Page',
			'id_gallery' => 'Gallery',
			'id_parent' => 'Parent',
            'visible' => 'Visible',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_post',$this->id_post);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('locked',$this->locked);
		$criteria->compare('id_type',$this->id_type);
		$criteria->compare('page',$this->page,true);
		$criteria->compare('id_gallery',$this->id_gallery);
		$criteria->compare('id_parent',$this->id_parent);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
    function search_by_user(User $user)
    {
		$criteria=new CDbCriteria;
        $criteria->together = true;
        $criteria->with = array('content','type');
        
        $criteria->condition = 'content.id_user = :idu';
        $criteria->params = array('idu'=>$user->id_user);
        
		$criteria->compare('id_type',$this->id_type);
		$criteria->compare('title',$this->title,true);

        $sort = new CSort();
        $sort->attributes = array(
            'title',
            'type.name',
            'content.date'
        );
        $sort->defaultOrder = 'content.date desc';
        
		return new CActiveDataProvider(Post::model(), array(
			'criteria'=>$criteria,
            'sort'=>$sort,
		));
    }
    
    private function render($edit=null)
    {
        if ( $edit == null )
            $edit = $this->content;
            
        if ( $this->type->name == 'page' )
            $output = ConverterFacade::o()->html_render($edit->text);
        else
            $output = nl2br(CHtml::encode($edit->text));
            
        return $output;
    }
    
    private function render_info($edit=null,$owner=null)
    {
        if ( $edit == null )
            $edit = $this->content;
            
        if ( $owner == null )
            $owner = $this->source->user;
        
        if ( $this->type->name == 'page' )
        {
            return "<div class='post-info'>".
                $owner->avatar_image(22) .
                "<div class='user-name'>".
                $owner->link() .
                "</div>" .
                "<div class='timestamp'>{$edit->date}</div>".
            "</div>";
        }
        else
        {
            return "<div class='post-info'>".
                $owner->avatar_image(32) .
                "<div class='user-name'>".
                $owner->link() .
                "<div class='user-role'>".$owner->role->name."</div>".
                "</div>" .
                "<div class='timestamp'>{$this->source->date}</div>".
            "</div>";
        }
    }
    
    private function begin_render($edit=null,$owner=null)
    {
        $output = "<div class='{$this->type->name}' id='post_$this->id_post' >".
                    $this->render_info($edit,$owner);
        
        if ( $this->type->name == 'page' )
        {
            $output .= '<h2 class="title" >'.$this->title.'</h2>';
        }
        
        return $output;
    }
    private function begin_content()
    {
        return '<div class="post">';
    }
    
    private function end_content()
    {
        return "</div>\n";
    }
    
    private function end_render()
    {
        return "</div>\n";
    }
    
    function full_render($return=false)
    {
        $output = $this->begin_render().$this->begin_content().$this->render();
        
        if ( $this->type->name != 'page' && count($this->edits) > 1 )
        {
            $output .= "<div class='timestamp'>Last edited on {$this->content->date}</div>";
        }
        $output .= $this->end_content();
        $output .= $this->render_edit_links();
        
        $output .= $this->end_render();
        
        if ( $return )
            return $output;
        else
            echo $output;
    }
    
    function render_edit_links()
    {
        if ( $this->type->name != 'wiki' )
        {
            if ( Yii::app()->user->has_role('admin') ||
                ( !$this->locked && Yii::app()->user->has_role('user') &&
                 Yii::app()->user->id == $this->source->author ) )
            {
                return '<div class="edit-post">'.$this->edit_link().'</div>';
            }
        }
        return '';
    }
    
    function render_edit($edit,$return=false)
    {
        $output = $this->begin_render($edit,$edit->user).
                    $this->begin_content().
                    $this->render($edit).
                    $this->end_content().
                    $this->end_render();
                    
        if ( $return )
            return $output;
        else
            echo $output;
    }
    
    function edit_check()
    {
        if ( !Yii::app()->user->has_role('admin') )
        {
            if ( $this->locked )
            {
                throw new CHttpException(403,
                            "This item has been locked by an administrator");
            }
            else if ( !Yii::app()->user->isGuest )
            {
                throw new CHttpException(401, 
                     CHtml::link('Log in',array('user/login'))
                    .' to reply' );
            }
            else if ( !Yii::app()->user->has_role('user') ||
                      Yii::app()->user->id != $this->source->author )
            {
                throw new CHttpException(403,
                            "You don't have the permission to edit this item");
            }
        }
    }
    
    function reply_form($button_text='Reply',$name='reply',$action=null)
    {
        if ( $this->locked )
        {
            echo '<div class="message">This topic has been closed</div>';
        }
        else if ( Yii::app()->user->isGuest )
        {
            echo '<div class="message">'.
                 CHtml::link('Log in',array('user/login'))
                .' to reply</div>';
        }
        else if ( !Yii::app()->user->has_role('user') )
        {
            echo '<div class="error">You cannot reply</div>';
        }
        else
        {
            echo '<form method="post"';
            if ( $action != null )
                echo ' action="'.CHtml::normalizeUrl($action).'"';
            echo '>';
            
            echo '<div class="row">';
            echo CHtml::textArea($name);
            echo '</div>';
            
            echo '<div class="row buttons">';
            echo CHtml::submitButton($button_text);
            echo '</div>';
            
            echo '</form>';
        }
    }
    
    function do_request_reply($type,$post_name='reply')
    {
        if ( isset($_POST[$post_name]) && Yii::app()->user->has_role('user')
            && !$this->locked )
        {
            $edit = $this->do_reply($_POST[$post_name], Yii::app()->user, $type);

            return $edit;
        }
        
        return null;
    }
    
    function do_reply($text,$user=null,$type=null)
    {
        if ( $user == null )
            $user = Yii::app()->user;
        if ( $type == null )
            $type = $this->type->name;
        
        $edit = new Edit;
        $edit->text = $text;
        
        if ( !$edit->validate(array('text')) )
        {
            return $edit;
        }
        // allow User and WebUser
        $edit->id_user = isset($user->id) ? $user->id : $user->id_user;
        $post = new Post;
        $post->title = "Re: ".$this->title;
        $post->id_type = PostType::findByName($type)->id_post_type;
        $post->id_parent = $this->id_post;
        $post->save(false);
        $edit->id_post = $post->id_post;
        
        if ( !$edit->save() )
            $post->delete();
        
        return $edit;
    }
    
    function url()
    {
        $t = $this->type->name;
        if ( $t == 'page' || $t == 'wiki' )
            return array("$t/view","page"=>$this->page);
        else if ( $t == 'ticket' || $t == 'forum' )
            return array("$t/comment","id"=>$this->id_post);
        else
            return array("$t/view","id"=>$this->id_post);
    }
    
    function link()
    {
        return CHtml::link($this->title,$this->url());
    }
    
    function edit_url()
    {
        $t = $this->type->name;
        if ( $t == 'page' || $t == 'wiki' )
            return array("$t/edit","page"=>$this->page);
        else if ( $t == 'ticket' || $t == 'forum' )
            return array("$t/editPost","id"=>$this->id_post);
        else
            return array("$t/edit","id"=>$this->id_post);
    }
    
    function edit_link()
    {
        return CHtml::link('Edit',$this->edit_url());
    }
    
    
}