<?php

class SearchForm extends CFormModel
{
    /// The post type to be searched on
    public $domains = array();
    public $title_only = false;
    /// What has to be searched
    public $term = '';
    public $username='';
    
    
	public function rules()
	{
		return array(
			array('title_only', 'boolean'),
            array('term, domains, username','safe'),
		);
	}
    
	public function attributeLabels()
	{
		return array(
			'part'=>'Post part',
			'domains'=>'Search in',
            'username' => "Author's name",
		);
	}
    
    function search()
    {
		$criteria=new CDbCriteria;
        $criteria->together = true;
        $criteria->with = array('content','content.user','type');
        
        
        $criteria->addSearchCondition('title',$this->term,true);
        
        if ( !$this->title_only )
            $criteria->addSearchCondition('content.text',$this->term,true, 'OR');
        
        if ( !empty($this->domains) )
            $criteria->addInCondition('id_type',$this->domains);
        
        if ( strlen(trim($this->username)) > 0 )
            $criteria->addSearchCondition('user.name',$this->username, true);
            
        $sort = new CSort();
        $sort->attributes = array(
            'title',
            'type.name',
            'content.date',
            'user.name',
        );
        $sort->defaultOrder = 'content.date desc';
        
		return new CActiveDataProvider(Post::model(), array(
			'criteria'=>$criteria,
            'sort'=>$sort,
		));
    }
}