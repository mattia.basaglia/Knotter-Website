<?php

/**
 * This is the model class for table "download".
 *
 * The followings are the available columns in table 'download':
 * @property integer $id_system
 * @property integer $id_architecture
 * @property integer $id_version
 * @property string $url
 * @property string $anchor
 *
 * The followings are the available model relations:
 * @property Architecture $architecture
 * @property System $system
 * @property Version $version
 */
class Download extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Download the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'download';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_system, id_architecture, id_version, url, anchor', 'required'),
			array('id_system, id_architecture, id_version', 'numerical', 'integerOnly'=>true),
			array('url, anchor', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_system, id_architecture, id_version, url, anchor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'architecture' => array(self::BELONGS_TO, 'Architecture', 'id_architecture'),
			'system' => array(self::BELONGS_TO, 'System', 'id_system'),
			'version' => array(self::BELONGS_TO, 'Version', 'id_version'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_system' => 'Id System',
			'id_architecture' => 'Id Architecture',
			'id_version' => 'Id Version',
			'url' => 'Url',
			'anchor' => 'Anchor',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_system',$this->id_system);
		$criteria->compare('id_architecture',$this->id_architecture);
		$criteria->compare('id_version',$this->id_version);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('anchor',$this->anchor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}