<?php

/**
 * This is the model class for table "ticket".
 *
 * The followings are the available columns in table 'ticket':
 * @property integer $id_ticket
 * @property integer $id_version
 * @property integer $id_type
 * @property integer $id_priority
 * @property integer $id_status
 * @property integer $id_assignee
 * @property integer $id_post
 * @property string $commit
 *
 * The followings are the available model relations:
 * @property Version $version
 * @property TicketType $type
 * @property TicketPriority priority
 * @property TicketStatus status
 * @property User $assignee
 * @property Post $post
 * @property Ticket $parent
 * @property Ticket[] $children
 */
class Ticket extends CActiveRecord
{
    
    public $searchTitle, $searchProject;
    
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Ticket the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ticket';
	}
    
    function init()
    {
        $this->id_priority = TicketPriority::findByName('normal')->id_ticket_priority;
        $this->id_status = TicketStatus::findByName('new')->id_ticket_status;
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_version, id_type, id_priority, id_status', 'required'),
			array('id_parent, id_version, id_type, id_priority, id_status, id_assignee',
                  'numerical', 'integerOnly'=>true),
            array('id_parent','compare','compareAttribute'=>'id_ticket',
                  'operator'=>'!=', 'allowEmpty'=>true,
                  'message'=>'Cannot be parent of itselt'),
			array('commit', 'length', 'max'=>40),
            array('id_post','safe'), // handled in controller
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('searchProject, searchTitle, id_ticket,
                  id_version, id_type, id_priority, id_status,
                  id_assignee, id_post, commit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'version' => array(self::BELONGS_TO, 'Version', 'id_version'),
			'type' => array(self::BELONGS_TO, 'TicketType', 'id_type'),
			'priority' => array(self::BELONGS_TO, 'TicketPriority', 'id_priority'),
			'status' => array(self::BELONGS_TO, 'TicketStatus', 'id_status'),
			'assignee' => array(self::BELONGS_TO, 'User', 'id_assignee'),
			'post' => array(self::BELONGS_TO, 'Post', 'id_post'),
			'parent' => array(self::BELONGS_TO, 'Ticket', 'id_parent'),
			'children' => array(self::HAS_MANY, 'Ticket', 'id_parent'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id_parent' => 'Parent',
			'id_version' => 'Version',
			'id_type' => 'Type',
			'id_priority' => 'Priority',
			'id_status' => 'Status',
			'id_assignee' => 'Assigned to',
			'id_post' => 'Post',
            'id_ticket' => 'Ticket Id',
			'commit' => 'Commit',
            'searchTitle' => 'Title',
            'searchProject' => 'Project',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($id_project=null)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id_version',$this->id_version);
		$criteria->compare('t.id_type',$this->id_type);
		$criteria->compare('t.id_priority',$this->id_priority);
		$criteria->compare('t.id_status',$this->id_status);
		$criteria->compare('t.id_assignee',$this->id_assignee);
		$criteria->compare('t.id_post',$this->id_post);
		$criteria->compare('t.commit',$this->commit,true);
        
        $criteria->with = array('post','version','priority','status');
        $criteria->compare('post.title',$this->searchTitle,true);
        
        if ( $id_project != null )
        {
            $criteria->compare('version.id_project',$id_project);
        }
        else
        {
            $criteria->compare('version.id_project',$this->searchProject);
            
        }


        $sort = new CSort();
        $sort->attributes = array(
            'id_type',
            'id_status',
            'searchTitle'=>array(
                'asc'=>'post.title ASC',
                'desc'=>'post.title DESC',
            ),
            'id_priority'=>array(
                'asc'=>'priority.weight desc',
                'desc'=>'priority.weight asc',
            ),
            'id_status'=>array(
                'asc'=>'status.weight asc',
                'desc'=>'status.weight desc',
            ),
        );
        $sort->multiSort = true;
        $sort->defaultOrder = 'status.weight asc, priority.weight desc';
        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => $sort,
		));
	}
    
    function full_title()
    {
        return $this->post->title." - #".$this->id_ticket;
    }
}