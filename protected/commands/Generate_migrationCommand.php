<?
class Generate_migrationCommand extends CConsoleCommand
{
    private $constraint_name_counter = 0;
    public $defaultAction = 'generate';
    
    public function getHelp()
    {
        return "\n".parent::getHelp().
            "Description:\n".
            "    Outputs the code needed by the starting migration script.\n"
            ;
    }
    
    /*function init($args=array())
    {
        $connection_name = 'db';
        $no_tables = true;
        foreach($args as $arg)
        {
            $m = array();
            if (preg_match('/^--connection-name=([a-zA-Z0-9_]+)$/',$arg,$m))
            {
                $connection_name = $m[1];
            }
            else if ( preg_match('/^[a-zA-Z0-9_]+$/',$arg) )
            {
                $this->actionTable($arg,$connection_name);
                $no_tables=false;
            }
        }
        if ( $no_tables )
            $this->actionGenerate($connection_name);
    }*/
    
    function actionGenerate($connection_name='db')
    {
        echo "\$fk_delete='CASCADE';\n\$fk_update='CASCADE';\n\n";
        
        $tables = Yii::app()->$connection_name->schema->getTables();
        foreach ($tables as $tab) {
            echo $this->dump_table($tab)."\n";
        }
    }
    
    function actionTable($name,$connection_name='db')
    {
        $table = Yii::app()->$connection_name->schema->getTable($name);
        if ( $table == null )
        {
            echo '';
            return 1;
        }
        echo $this->dump_table($table);
    }
    
    /**
     * runs actionGenerate then actionPopulate for each CSV model name
     */
    function actionBatch($csv,$connection_name='db')
    {
        $models = explode(",",$csv);
        $this->actionGenerate($connection_name);
        foreach($models as $model)
        {
            $this->actionPopulate(trim($model));
        }
    }
    
    function actionPopulate($model)
    {
        $all = $model::model()->findAll();
        echo "// Populating ".$model::model()->tableName()."\n";
        foreach($all as $m)
        {
            echo "\$model = new $model();\n";
            foreach($m->attributes as $n => $a)
            {
                echo "\$model->$n = '".addslashes($a)."';\n";
            }
            echo "\$model->save(false);\n\n";
        }
    }
    
    private function constraint($table=null,$name='fk')
    {
        $this->constraint_name_counter++;
        if ( $table != null )
            $name = $table->name."_$name";
        return "{$name}_constraint_$this->constraint_name_counter";
    }
    
    private function dump_table(CDbTableSchema$tab)
    {
        echo "// Generating $tab->name\n";
        $result = '$this->createTable("'.$tab->name.'", array('."\n";
        
        foreach ($tab->columns as $col)
            $result .= "    '{$col->name}' => '".
                        $this->dump_column($col)."',\n";
        
        $columns = $tab->primaryKey;
        if ( is_array($columns) )
        {
            foreach ( $columns as &$col )
                $col = "`$col`";
            $columns = implode(',',$columns);
        }
        else
            $columns = "`$columns`";
        $result .= "'PRIMARY KEY ($columns)',\n";
        
        $result .= "));\n";
        
        /*$result .= "\$this->addPrimaryKey('".$this->constraint($tab,'pk').
                        "', '{$tab->name}', '$columns');\n";*/
        
        foreach ( $tab->foreignKeys as $col => $fk )
            $result .= "\$this->addForeignKey('".$this->constraint($tab,'fk').
                        "', '{$tab->name}', '$col', \n".
                        "    '{$fk[0]}', '{$fk[1]}', ".
                        "\$fk_delete, \$fk_update);\n";
        
        
        return $result;
    }
 
    private function dump_column(CDbColumnSchema$col)
    {
        $result = $col->dbType;
        
        if (!$col->allowNull)
            $result .= ' NOT NULL';
            
        if ($col->defaultValue != null)
            $result .= " DEFAULT \'".addslashes($col->defaultValue)."\'";
            
        if ( $col->autoIncrement )
            $result .= " AUTO_INCREMENT";
            
        if ( !empty($col->comment) )
             $result .= " COMMENT \'".addslashes($col->comment)."\'";
        
        return $result;
    }
} 
