<?php

class Toolbox extends CWidget
{
    
    public $title = '';
    public $showTitle = true;
    public $collapsible = true;
    public $collapsed = false;
    public $cssFile = null;
    public $cssClass = 'toolbox';
    public $titleLevel = 3;
    
    public $linkUrl = null;
    public $linkText = 'View';
    
    function titleElement($level,$text,$htmlOptions=array())
    {
        return CHtml::tag("h".min($level+$this->titleLevel,6),
                          $htmlOptions, $text );
    }
    
    function init()
    {
        if ( $this->cssFile == null )
            $this->cssFile = CHtml::normalizeUrl(array('main/css',
                                                       'sheet'=>'toolbox'));
        if ( $this->cssFile != false )
            Yii::app()->clientScript->registerCssFile($this->cssFile);
        
        
        echo "<div class='$this->cssClass' id='$this->id'>";
        
        
        $title_id = $this->id."_title";
        $content_id = $this->id."_content";
        
        if ( $this->linkUrl != null )
        {
            echo CHtml::link($this->linkText,$this->linkUrl);
        }
        
        if ( strlen(trim($this->title)) > 0 && $this->showTitle )
        {
        
            echo $this->titleElement(0,$this->title,
                            array( 'class'=>'title', 'id' => $title_id ) );
            
            
            if ( $this->collapsible )
            {
                Yii::app()->clientScript->registerScript($this->id."_collapse",
                    "$('#$title_id').click(function(){".
                    "$('#$content_id').toggle('slow')".
                    "})"
                );
            }
        }
        
        
        echo "<div id='$content_id' ";
        if ( $this->collapsed )
            echo 'style="display:none"';
        echo ">";
    }
    
    function run()
    {
        echo '</div></div>';
    }
}