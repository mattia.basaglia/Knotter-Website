<?php
require_once(dirname(__FILE__)."/BasicConverter.php");
require_once(dirname(__FILE__)."/Element_Html_Markdown.php");

/**
 * Custom filter, a single line is passed
 */
interface MarkdownLineFilter
{
    /**
     * Do processing on the line; return element, HTML string or null 
     */
    function process_line($line,MarkdownConverter $converter);
}

class MarkdownConverter extends BasicConverter
{
    /// Array of MarkdownLineFilter, intended for block-level filters
    public $block_filters = array();
    
    public $element_class = 'Element_Html_Markdown';
    
    /// lines to be processed
    private $lines = array();
    
    function push_element($element)
    {
        if ( $this->current_element() != null &&
            $this->current_element()->name == 'p' &&
            $element->is_block() )
        {
            $this->pop_element();
        }
        parent::push_element($element);
    }
    
    /**
     * Process block filters
     *
     * @return boolean  FALSE if the line does not match any filter
     */
    private function process_block_filters($line)
    {
        foreach ( $this->block_filters as $filter )
        {
            $r = $filter->process_line($line,$this);
            if ( is_string($r) )
            {
                $this->push_html($r);
                return true;
            }
            else if ( $r !== null )
            {
                $this->push_element($r);
                return true;
            }
        }
        return false;
    }
    
    
    /**
     * Process list items
     *
     * @return boolean  FALSE if the line is not a list item
     */
    private function process_list_item($line)
    {   
        $subpattern = array();
        
        if ( preg_match('/^([-*#]+)\s+/',$line,$subpattern) )
        {
            $depth = strlen($subpattern[1]);
            
            $type = 'ul';
            if ( $subpattern[1][$depth-1] == '#' )
                $type = 'ol';
            
            $container = $this->current_element();
            $detected_depth = 0;
            while($container != null)
            {
                if ( $container->is_list_container() )
                    $detected_depth++;
                $container = $container->parent;
            }
            
            if ( $depth > $detected_depth )
            {
                if ( $this->current_element()->name == 'p' )
                    $this->pop_element();
                    
                for ( $i = $depth-$detected_depth; $i > 0; $i-- )
                    $this->push_element($this->create_element($type));
            }
            else if ( $depth < $detected_depth )
            {
                $delta = $detected_depth - $depth;
                while($delta > 0)
                {
                    if ( $this->current_element()->is_list_container() )
                        $delta--;
                    $this->pop_element();
                }
            }
            
            $this->push_element( $this->create_element('li') );
            $this->process_inline(substr($line,strlen($subpattern[0])));
            $this->pop_element();
            
            return true;
        }
        return false;
    }
    
    /**
     * Process verbatim blocks
     *
     * @return boolean  FALSE if the line is not a verbatim environment
     */
    private function process_verbatim($line)
    {   
        $subpattern = array();
        
        if ( preg_match("/^    |\t/",$line,$subpattern) )
        {
            if ( $this->current_element()->name != 'pre' )
            {
                $this->push_element($this->create_element('pre'));
            }
            
            //$this->process_inline(substr($line,strlen($subpattern[0])));
             $this->current_element()->push_plaintext(
                        substr($line,strlen($subpattern[0]))."\n"
                    );
            return true;
        }
            
        return false;
    }
    
    
    protected function init()
    {
        $this->open_elements=array();
        $this->current_line = 1;
    }
    /**
     * Process quote blocks
     *
     * @return boolean  FALSE if the line is not a quote block
     */
    private function process_blockquote($line)
    {   
        $subpattern = array();
        
        if ( preg_match('/^((>\s+)+)/',$line,$subpattern) )
        {
            $depth = strlen(preg_replace('/\s/','',$subpattern[1]));
            
            $container = $this->current_element();
            $detected_depth = 0;
            while($container != null)
            {
                if ( $container->name == 'blockquote' )
                    $detected_depth++;
                $container = $container->parent;
            }
            
            if ( $depth > $detected_depth )
            {
                if ( $this->current_element()->name == 'p' )
                    $this->pop_element();
                    
                for ( $i = $depth-$detected_depth; $i > 0; $i-- )
                    $this->push_element($this->create_element('blockquote'));
            }
            else if ( $depth < $detected_depth )
            {
                $delta = $detected_depth - $depth;
                while($delta > 0)
                {
                    if ( $this->current_element()->name == 'blockquote' )
                        $delta--;
                    $this->pop_element();
                }
            }
            
            $this->push_element($this->create_element('p'));
            $this->process_inline(substr($line,strlen($subpattern[0])));
            $this->pop_element();
            
            return true;
        }
            
        return false;
    }
    
     
    /**
     * Process headers
     *
     * @return boolean  FALSE if the line is not a header
     */
    private function process_header($line)
    {
        $subpatterns = array();
        if ( preg_match('/^(#+)[^#]+\1\s*$/',$line,$subpatterns) )
        {
            $hn = $this->create_element('h'.(strlen($subpatterns[1])+1));
            $hn->attributes['class'] = 'title';
            $this->push_element($hn);
            $this->process_inline(trim(ltrim($line),'#'));
            $this->pop_element();
            return true;
        }
        return false;
    }
     
    /**
     * Process inline contents
     *
     * @return boolean  FALSE if the line is not inline content
     */
    private function process_inline($line)
    {
        $line = $this->convert_line($line);
        $this->current_element()->push_html($line);
        return true;
    }
    
    /**
     * Transforms inline parts of Markdown
     */
    private function convert_line($line)
    {
        $line = $this->convert_images($line);
        $line = $this->convert_links($line);
        $line = $this->convert_inline_element('**','strong',$line);
        $line = $this->convert_inline_element('__','strong',$line);
        $line = $this->convert_inline_element('*','em',$line);
        //$line = $this->convert_inline_element('_','em',$line);
        $line = $this->convert_inline_element('``','code',$line);
        return $line;
    }
    
    private function convert_inline_element($md_delimiter,$html_name,$line)
    {
        $d = preg_quote($md_delimiter);
        $sub = array();
        if ( preg_match_all('/'.$d.'(.+)'.$d.'/U',$line,$sub) )
        {
            for($i = 0; $i < count($sub[0]); $i++)
            {
                $element = $this->create_element($html_name);
                $element->push_html($this->convert_line($sub[1][$i]));
                $line = str_replace($sub[0][$i],$element->process(),$line);
            }
        }
        return $line;
        
    }

    
    private function convert_images($line)
    {
        $sub = array();
        if ( preg_match_all('/!\[([^]]+)\]\(([^)]+)\)/',$line,$sub) )
        {
            for($i = 0; $i < count($sub[0]); $i++)
            {
                $img = $this->create_element('img');
                $img->attributes['alt'] = $sub[1][$i];
                $img->attributes['src'] = $sub[2][$i];
                $line = str_replace($sub[0][$i],$img->process(),$line);
            }
        }
        return $line;
    }
    
    private function convert_links($line)
    {
        $sub = array();
        if ( preg_match_all('/\[([^]]+)\]\(([^)]+)\)/',$line,$sub) )
        {
            for($i = 0; $i < count($sub[0]); $i++)
            {
                $a = $this->create_element('a');
                $a->attributes['href'] = $sub[2][$i];
                $a->push_html( $this->convert_line($sub[1][$i]) );
                $line = str_replace($sub[0][$i],$a->process(),$line);
            }
        }
        return $line;
    }
    
    private function scan_lines()
    {
        $this->push_element($this->create_element('p'));
        while ( !empty($this->lines) )
        {
            $first = array_shift($this->lines);
            
            if ( empty($first) )
            {
                $this->flush_elements();
                $this->push_element($this->create_element('p'));
            }
            else
            {
                if ( !empty($this->lines) )
                {
                    $lookahead = $this->lines[0];
                    if ( preg_match('/^[=]+\s*$/',$lookahead) )
                    {
                        array_shift($this->lines);
                        $h2 = $this->create_element('h2');
                        $h2->attributes['class'] = 'title';
                        $this->push_element($h2);
                        $this->process_inline($first);
                        continue;
                    }
                    else if ( preg_match('/^[-]+\s*$/',$lookahead) )
                    {
                        array_shift($this->lines);
                        $h3 = $this->create_element('h3');
                        $h3->attributes['class'] = 'title';
                        $this->push_element($h3);
                        $this->process_inline($first);
                        continue;
                    }
                }
                
                $this->process_block_filters($first) or
                $this->process_header($first)        or
                $this->process_list_item($first)     or
                $this->process_blockquote($first)    or
                $this->process_verbatim($first)      or
                $this->process_inline($first)        ;  
            }
        }
    }
    
    function convert($text)
    {
        $this->init();
        $this->lines = explode("\n",$text);
        $this->create_root();
        $this->scan_lines();
        
        if ( $this->current_element() === null )
        {
            $this->error("Parsing failed");
            return null;
        }
        
        $this->flush_elements();
        
        return $this->current_element()->process();
    }
}