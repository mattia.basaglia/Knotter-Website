<?php
require_once(dirname(__FILE__)."/ConverterElement.php");

/**
 * Base for any ConverterElement that outputs HTML
 */
class BasicHtmlConverterElement extends ConverterElement
{
    /// Whether the element was self-closing in the source file
    public $self_closing_source = false;
    
    /**
     * Whether the element should be like <this /> rather than like <this></this>
     */
    public function is_self_closing()
    {
        return $this->self_closing_source;
    }
    
    
    function process()
    {
        $txt = "<$this->name";
        foreach ( $this->attributes as $att => $val )
        {
            $txt .= " $att='".htmlspecialchars($val,ENT_QUOTES,'UTF-8',false)."'";
        }
        if ( $this->is_self_closing() )
        {
            $txt .= " />";
        }
        else
            $txt .= ">$this->contents</$this->name>";
        
        return $txt;
    }
    
    /**
     * Append pre-processed html content (does no escaping)
     */
    function push_html($text)
    {
        $this->contents .= $text;
    }
    
    function push_plaintext($text)
    {
        $this->contents .= htmlspecialchars($text);
    }
    
    function push_element($element)
    {
        $this->contents .= $element->process();
    }
    
}