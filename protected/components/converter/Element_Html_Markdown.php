<?php
require_once(dirname(__FILE__)."/BasicHtmlConverterElement.php");

/**
 * Used to generate HTML from Markdown
 */
class Element_Html_Markdown extends BasicHtmlConverterElement
{
    /// ouput only contents
    static public $ignore_tags = array(
        'body',
    );
    /// ouput <name />
    static public $self_closing = array(
        'img',
    );
    /// Used to add a space at the end
    static public $block_elements = array(
        'h1','h2','h3','h4','h5','h6',
        'p','div','pre',
        'ul','ol','li',
    );
    
    static public $list_container = array(
        'ul', 'ol'
    );
    
    /// whether should output tags if content is empty
    public $keep_if_empty = false;
    
    public function is_list_container()
    {
        return in_array($this->name,self::$list_container);
    }
    
    
    function is_block()
    {
        return in_array($this->name,self::$block_elements);
    }
    
    function is_self_closing()
    {
        return in_array($this->name,self::$self_closing);
    }
    
    
    function process()
    {
        if ( !$this->keep_if_empty  && !$this->is_self_closing() &&
                strlen(trim($this->contents)) == 0 )
        {
            return '';
        }
        
        if ( in_array($this->name,self::$ignore_tags) )
            return $this->contents;
        
        if ( in_array($this->name,self::$self_closing) )
            $this->self_closing = true;
        
        $txt = parent::process();
        if ( $this->is_block() )
            $txt .= "\n";
        return $txt;
    }
};