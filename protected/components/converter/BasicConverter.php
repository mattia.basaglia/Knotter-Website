<?php

abstract class BasicConverter
{
    public $element_class;
    
    /// array of currently open elements
    private $open_elements = array();
    /// array of errors [n] => array('what'=>text, 'line'=>number)
    public $errors = array();
    
    /// Current line number in stream
    protected $current_line = 0;
    
    public function create_element($name='')
    {
        $class_name = $this->element_class;
        $element = new $class_name();
        $element->name = $name;
        return $element;
    }
    
    /**
     * Append an error to the list
     *
     * Line number is deduced automatically
     * 
     * @param string $what Error message
     */
    public function error($what)
    {
        $this->errors []= array(
            'what'=>$what,
            'line'=>$this->current_line
        );
    }
    /**
     * Removes all errors
     */
    function clear_errors()
    {
        $this->error = array();
    }
    
    
    /**
     * Append text to last open element
     */
    protected function add_plaintext($text)
    {
        if(empty($this->open_elements))
        {
            $this->error('Missing root element');
            $this->create_root();
        }
        
        $this->open_elements[count($this->open_elements)-1]
            ->push_plaintext ( $text );
    }
    /**
     * Append  processed text to last open element
     */
    protected function add_element_text($element)
    {
        if(empty($this->open_elements))
        {
            $this->error('Missing root element');
            $this->create_root();
        }
        
        $this->open_elements[count($this->open_elements)-1]
            ->push_element($element);
    }
    
    /**
     * Creates a root element
     */
    protected function create_root($name='body')
    {
        $this->push_element($this->create_element($name));
    }
    
    /**
     * Add an element to the stack
     */
    protected function push_element($element)
    {
        if ( !empty($this->open_elements) )
            $element->parent = $this->open_elements[count($this->open_elements)-1];
        $this->open_elements []= $element;
    }
    
    /**
     * Return currently open element
     */
    public function current_element()
    {
        if ( empty($this->open_elements) )
            return null;
        return $this->open_elements[count($this->open_elements)-1];
    }
    
    /**
     * Remove last element and add text
     * @pre !empty($this->open_elements)
     */
    protected function pop_element()
    {
        if ( count($this->open_elements) <= 1 )
        {
            $this->error("Trying to pop root element");
            return;
        }
        $last = array_pop($this->open_elements);
        $this->add_element_text($last);
    }
    
    /**
     * Removes elements till there is only the root
     */
    protected function flush_elements()
    {
        while( count($this->open_elements) != 1 )
            $this->pop_element();
    }
    
    abstract function convert($text);
    
    /**
     * Clears parser status
     */
    protected function init()
    {
        $this->open_elements=array();
        $this->create_root();
        $this->current_line = 1;
    }
}