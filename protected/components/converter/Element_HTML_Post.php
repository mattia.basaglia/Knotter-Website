<?php
require_once(dirname(__FILE__)."/BasicHtmlConverterElement.php");

/**
 * Pre-process HTML from Database
 */
class Element_HTML_Database_revert extends BasicHtmlConverterElement
{
    /*
     * Expands $url$ to the application base URL
     */
    public function expand_url($text)
    {
        return str_replace('$url$',Controller::base_url(),$text);
    }
    
    function process()
    {
        if ( $this->name == 'body' )
            return $this->contents;
        
        if ( $this->name == 'a' )
        {
            if ( isset($this->attributes['href']) )
            {
                $this->attributes['href'] =
                    $this->expand_url($this->attributes['href']);
            }
        }
        else if ( $this->name == 'img' )
        {
            if ( isset($this->attributes['src']) )
            {
                $this->attributes['src'] =
                    $this->expand_url($this->attributes['src']);
            }
        }
        
        return parent::process();
    }
}

/**
 * Post-process HTML, applies custom widgets but does not do any other filtering
 */
class Element_HTML_Post extends Element_HTML_Database_revert
{
    /*
     * Expands $url$ to the application base URL
     */
    public function expand_url($text)
    {
        return str_replace('$url$',Controller::base_url(),$text);
    }
    
    function process()
    {
        if ( $this->name == 'body' )
            return $this->contents;
        if ( $this->name == 'div' )
        {
            if ( isset($this->attributes['data-gallery']) )
            {
                $gallery = Gallery::model()->findByPk(
                                        $this->attributes['data-gallery']);
                if ( $gallery == null )
                {
                    $this->attributes['class'] = 'error';
                    $this->contents = "Wrong gallery ID";
                }
                else
                {
                    $gr = new GalleryRenderer();
                    $gr->gallery = $gallery;
                    $gr->item_height = 128;
                    ob_start();
                    ob_implicit_flush(false);
                    $gr->init();
                    $gr->run();
                    return ob_get_clean();
                }
            }
        }
        else if ( $this->name == 'a' )
        {
            if ( isset($this->attributes['href']) )
            {
                $this->attributes['href'] =
                    $this->expand_url($this->attributes['href']);
            }
        }
        else if ( $this->name == 'img' )
        {
            if ( isset($this->attributes['src']) )
            {
                $this->attributes['src'] =
                    $this->expand_url($this->attributes['src']);
            }
        }
        
        return parent::process();
    }
}