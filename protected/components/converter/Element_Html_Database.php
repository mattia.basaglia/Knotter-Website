<?php
require_once(dirname(__FILE__)."/BasicHtmlConverterElement.php");

/**
 * Used to filter post in order to make them less domain-specific
 */
class Element_Html_Database extends BasicHtmlConverterElement
{
    /*
     * Compress application base URL to $url$
     */
    public function compress_url($text)
    {
        return str_replace(Controller::base_url(),'$url$',$text);
    }
    
    function process()
    {
        if ( $this->name == 'body' )
            return $this->contents;
        
        if ( $this->name == 'a' )
        {
            if ( isset($this->attributes['href']) )
            {
                $this->attributes['href'] =
                    $this->compress_url($this->attributes['href']);
            }
        }
        else if ( $this->name == 'img' )
        {
            if ( isset($this->attributes['src']) )
            {
                $this->attributes['src'] =
                    $this->compress_url($this->attributes['src']);
            }
        }
        
        return parent::process();
    }
}