<?php
require_once(dirname(__FILE__)."/BasicHtmlConverterElement.php");

/**
 * Used to generate Fitered HTML
 */
class Element_HTML_Filtered extends BasicHtmlConverterElement
{
    /// Elements not on this list will be only output their contents
    static public $allowed_elements = array(
        'p','pre','blockquote','div',
        'a','img',
        'strong','em','code',
        'h3','h4','h5','h6'
    );
    
    /// converts name before output
    static public $alias = array(
        'b'=>'strong',
        'tt'=>'code',
        'i'=>'em',
    );
    
    /// ouput <name />
    static public $self_closing = array(
        'img',
    );
    
    /// For each element, list attributes that can be read
    static public $allowed_attributes = array(
        'a'=>array('href','title'),
        'img'=>array('src','title','alt','width','height'),
        'div'=>array('class'=>array('error','message','success','note')),
    );
    
    /// if the attribute is not specified, it will get this value
    static public $default_attributes = array(
        'img'=>array('alt'=>'',
                     'src'=>'$url$/img/icon/scalable/image-not-found.svg'),
    );
    
    /// these attributes are always applied, no matter what
    static public $fixed_attributes = array(
        'h3'=>array('class'=>'title'),
        'h4'=>array('class'=>'title'),
        'h5'=>array('class'=>'title'),
        'h6'=>array('class'=>'title'),
    );
    
    
    protected function fix_attributes()
    {
        $filtered_attrs = array();
        
        if ( isset(self::$default_attributes[$this->name]) )
        {
            foreach(self::$default_attributes[$this->name] as $name => $value)
            {
                $filtered_attrs[$name] = $value;
            }
        }
        
        if ( isset(self::$allowed_attributes[$this->name]) )
        {
            foreach( self::$allowed_attributes[$this->name] as $index => $value)
            {
                // fixed values only
                if ( is_array($value) )
                {
                    if ( isset($this->attributes[$index]) &&
                            in_array($this->attributes[$index],$value) )
                        $filtered_attrs[$index] = $this->attributes[$index];
                }
                else if ( isset($this->attributes[$value]) )
                {
                    $filtered_attrs[$value] = $this->attributes[$value];
                }
            }
        }
        
        if ( isset(self::$fixed_attributes[$this->name]) )
        {
            foreach(self::$fixed_attributes[$this->name] as $name => $value)
            {
                $filtered_attrs[$name] = $value;
            }
        }
        
        $this->attributes = $filtered_attrs;
    }
    
    function is_self_closing()
    {
        return in_array($this->name,self::$self_closing);
    }
    
    function process()
    {
        // filter out unwanted elements
        if ( !in_array($this->name, self::$allowed_elements) )
            return $this->contents;
        
        $this->fix_attributes();
        
        return parent::process();
    }
}

