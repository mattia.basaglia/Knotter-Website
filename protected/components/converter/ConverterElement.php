<?
/**
 * Class representing HTML element
 */
abstract class ConverterElement
{
    /// element name (div, span etc.)
    public $name;
    /// attribute name=>value
    public $attributes = array();
    /// plain text contents
    protected $contents='';
    /// parent element
    public $parent='';
    
    /**
     * return converted string from $contents
     */
    abstract function process();
    
    /**
     * Process $text and append to $contents
     */
    abstract function push_plaintext($text);
    
    /**
     * Append $element to $contents
     */
    abstract function push_element($element);
    
    /**
     * Returns the current contents
     */
    function get_contents()
    {
        return $this->contents;
    }
}
