<?php
require_once(dirname(__FILE__)."/ConverterElement.php");
/**
 * Used to generate markdown
 */
class Element_Markdown extends ConverterElement
{
    /// adds value before and after the text
    static public $plain_elements = array(
        'h4'=>'###',
        'h5'=>'####',
        'h6'=>'#####',
        'b'=>'**',
        'strong'=>'**',
        'em'=>'*',
        'i'=>'*',
        'code'=>'``',
        'tt'=>'``',
    );
    /// main title elemens (adds a line below)
    static public $title_elements = array(
        'h1'=>'=',
        'h2'=>'=',
        'h3'=>'-',
    );
    
    /// appends a blank line (two newline characters)
    static public $separated_block_elements = array(
        'h1','h2','h3','h4','h5','h6',
        'p','div',
    );
    /// one per line prepends characters depending on parent
    static public $list_elements = array(
        'li'=>array('ul'=>'*','ol'=>'#'),
    );
    static public $list_container = array(
        'ul', 'ol',
    );
    
    /// don't collapse spaces
    static public $verbatim_elements = array(
        'pre',
    );
    
    /// prepend text to each line 
    static public $multiline_elements = array(
        'blockquote' => '> ',
        'pre' => '    ',
    );
    
    /// Used to skip space around tags
    static public $block_elements = array(
        'h1','h2','h3','h4','h5','h6',
        'p','div',
        'ul','ol','li',
    );
    
    
    /// Used to add a space before and after
    static public $inline_elements = array(
        'b','strong','em','i','code','tt'
    );
    /**
     * Call the callback passing the current element
     * If the callback returns null, the usual processing is performed
     */
    static public $special_template = array(
        'img'=>array(__CLASS__,'process_img'),
        'a' => array(__CLASS__,'process_a'),
    );
    
    public static function process_img($element)
    {
        $txt = " ![";
        if ( isset($element->attributes['alt']) )
            $txt .= $element->attributes['alt'];
        $txt .= "](";
        if ( isset($element->attributes['src']) )
            $txt .= $element->attributes['src'];
        if ( isset($element->attributes['title']) )
            $txt .= ' "'.$element->attributes['title'].'"';
        $txt .= ") ";
        return $txt;
    }
    // todo compress url
    public static function process_a($element)
    {
        $txt = " [".$element->get_contents()."](";
        if ( isset($element->attributes['href']) )
            $txt .= $element->attributes['href'];
        if ( isset($element->attributes['title']) )
            $txt .= ' "'.$element->attributes['title'].'"';
        $txt .= ") ";
        return $txt;
    }
    
    function is_block()
    {
        return in_array($this->name,self::$block_elements);
    }
    
    function is_inline()
    {
        return in_array($this->name,self::$inline_elements);
    }
    
    function is_verbatim()
    {
        return in_array($this->name,self::$verbatim_elements);
    }
    
    private $ends_ws = false;
    private $ends_inl = false;
    
    function push_plaintext($text)
    {
        $text = htmlspecialchars($text);
        
        if ( !$this->is_verbatim() )
        {
            $this->ends_ws = ctype_space(substr($text,-1));
            
            if ( !$this->is_inline() )
            {
                if ( $this->ends_inl )
                    $text = rtrim($text);
                else
                    $text = trim($text);
            }
            $text = preg_replace("/\s+/",' ',$text);
        }
        
        $this->contents .= $text;
    }
    
    function push_element($element)
    {
        if ( in_array($this->name,self::$list_container) &&
                isset(self::$list_elements[$element->name]) )
        {
            $this->contents .= "\n";
        }
        
        $this->ends_inl = $element->is_inline();
        if ( $this->ends_ws && $this->ends_inl && !$this->is_verbatim() )
        {
            $this->contents .= ' ';
        }
        $this->contents .= $element->process();
    }
    
    function process()
    {
        $text = $this->contents;
        
        if ( isset(self::$special_template[$this->name]) )
        {
            $callback = self::$special_template[$this->name];
            $result = call_user_func($callback,$this);
            if ( $result !== null )
                return $result;
        }
        
        if ( isset(self::$plain_elements[$this->name]) )
        {
            $c = self::$plain_elements[$this->name];
            $text = $c.$text.$c;
        }
        
        if ( isset(self::$title_elements[$this->name]) )
        {
            $c = self::$title_elements[$this->name];
            $text .= "\n".str_repeat($c,strlen($text));
        }
        
        if ( isset(self::$multiline_elements[$this->name]) )
        {
            $prefix = self::$multiline_elements[$this->name];
            $lines = explode("\n",trim($text));
            $text = "";
            foreach ( $lines as $l )
                $text .= "$prefix$l\n";
            return "$text\n";
        }
        
        if (  isset(self::$list_elements[$this->name]) )
        {
            $config = self::$list_elements[$this->name];
            
            $bullet = '*';
            $nested = 0;
            
            if($this->parent != null)
            {
                $bullet = $config[$this->parent->name];
            }
            
            $p = $this->parent;
            while ( $p != null )
            {
                if ( isset($config[$p->name]) )
                    $nested ++;
                else if ( $p->name != $this->name )
                    break;
                $p = $p->parent;
            }
            
            $text = str_repeat($bullet,$nested)."  $text";
        }
        else if ( in_array($this->name,self::$list_container) )
        {
            $pn = $this->parent->name;
            if ( !in_array($pn,self::$list_container) &&
                !isset(self::$list_elements[$pn]) )
                $text .= "\n\n";
        }
        else if ( in_array($this->name,self::$separated_block_elements) ||
            isset(self::$title_elements[$this->name]) ||
            !isset($this->parent) )
        {
            $text .= "\n\n";
        }
        else if ( $this->is_block() )
        {
            $text .= "\n";
        }
        
        return $text;
    }
}


