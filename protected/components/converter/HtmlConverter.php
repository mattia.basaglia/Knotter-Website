<?php
require_once(dirname(__FILE__)."/BasicConverter.php");

class HtmlConverter extends BasicConverter
{
    /// Class used to represent HTML elements and perform the conversion
    public $element_class='Element_HTML_Filtered';
    
    /// Stream on which input occurs
    private $stream;
    /// Extra character that may be read during some scan operations
    private $lookahead = null;
    
    /**
     * Skips HTML comments
     *
     * @pre scanned <!
     */
    private function skip_comment()
    {
        $this->scan_char(); 
        $this->scan_char();// skip --
        
        while ( true )
        {
            $c = $this->scan_char();
            if ( $c === false )
                return;
            // this works because -- is not allowed.
            // things like ---> won't work
            if ( $c == '-' && $this->scan_char() == '-' &&
                    $this->scan_char() == '>' )
                return;
        }
    }
    
    /**
     * Detects HTML tag and creates a new Element or calls close_tag if a
     * closing tag is detected
     *
     * @pre scanned <
     */
    private function open_tag()
    {
        $c = $this->scan_char();
        if ( $c === false || ( $c != '/' && $c != '!' && !$this->ctype_id($c) ) )
        {
            $this->error('Bad tag opening');
            return;
        }
        if ( $c == '/' )
            $this->close_tag();
        else if ( $c == '!' )
            $this->skip_comment();
        else
        {
            $element = $this->create_element($this->scan_id($c));
            
            while(true)
            {
                $c = $this->scan_char_nows();
                if ( $this->ctype_id($c) )
                {
                    $name = $this->scan_id($c);
                    $c = $this->scan_char_nows();
                    if ( $c == '=' )
                    {
                        $c = $this->scan_char_nows();
                        if ( $c == "'" || $c == '"' )
                        {
                            $value = $this->scan_quoted_text($c);
                            $element->attributes[$name] = $value;
                            continue;
                        }
                    }
                    $this->lookahead = $c;
                    $this->error("Please use XML-style attributes for $element->name");
                }
                else if ( $c == '/' )
                {
                    if ( $this->scan_char() == '>' )
                    {
                        if ( isset($element->self_closing_source) )
                            $element->self_closing_source = true;
                        $self_closing = true;
                        break;
                    }
                    $this->error("Odd character in element $element->name");
                    return;
                }
                else if ( $c == '>' )
                {
                    $self_closing = false;
                    break;
                }
                else
                {
                    $this->error("Corrupted element $element->name");
                    return;
                }
            }
            
            if ( $self_closing )
                $this->add_element_text($element);
            else
            {
                $this->push_element($element);
            }
        }
    }
    
    /**
     * Checks integrity and pops last element
     *
     * @pre scanned </
     */
    private function close_tag()
    {
        $name = $this->scan_id();
        
        $c = $this->scan_char();
        
        if ( $c != '>' )
        {
            $this->error("Weird closing tag </$name");
            return;
        }
        
        $curr = $this->current_element();
        if( $curr == null || $curr->name != $name )
        {
            $this->error("Unmatched tag </$name>");
            return;
        }
        $this->pop_element();
    }
    
    /**
     * Returns whether the stream has no more input
     */
    private function eof()
    {
        return feof($this->stream);
    }
    
    /**
     * Read a single character from the stream
     *
     * If $lookahead is not null will return that
     *
     * Increases $current_line when a newline is encountered
     */
    private function scan_char()
    {
        if ( $this->lookahead === null )
        {
            $c = fgetc($this->stream);
            if ( $c == "\n" )
                $this->current_line++;
            return $c;
        }
        else
        {
            $c = $this->lookahead;
            $this->lookahead = null;
            return $c;
        }
    }
    
    /**
     * Read a single character disregarding any leading whitespaces
     */
    private function scan_char_nows()
    {
        do
        {
            $c = $this->scan_char();
        }
        while ( ctype_space($c) );
        return $c;
    }
    
    /**
     * Returns true if $char matches a valid HTML element/attribute name
     */
    private function ctype_id($char)
    {
        return preg_match("/[-_a-zA-Z0-9]/",$char);
    }
    
    /**
     * Reads an HTML element/attribute name
     *
     * @param string $prefix Prefix to be added to the identifier
     */
    private function scan_id($prefix='')
    {
        $c = null;
        $id = $prefix;
        
        while(true)
        {
            $c = $this->scan_char();
            if ( $this->ctype_id($c) )
                $id .= $c;
            else
            {
                $this->lookahead=$c;
                return $id;
            }
        }
    }
    
    /**
     * Return string from current position up to $delim
     */
    private function scan_quoted_text($delim)
    {
        $c = null;
        $txt = '';
        
        while(true)
        {
            $c = $this->scan_char();
            if ( $c == $delim || $c === false )
                return $txt;
            $txt .= $c;
        }
    }
    
    
    /**
     * Scans HTML text
     */
    private function scan_html()
    {
        $c = null;
        $txt = '';
        while(true)
        {
            $c = $this->scan_char();
            if ( $c === false )
                return;
            else if ( $c == '<' )
            {
                $this->add_plaintext($txt);
                $txt = '';
                $this->open_tag();
            }
            else
                $txt .= $c;
        }
    }
    
    /**
     * Converts input HTML as defined by Element::process()
     */
    function convert($text)
    {
        $this->init();
        $this->lookahead = null;
        $this->stream = fopen('data://text/plain,' . $text,'r');
        $this->scan_html();
        
        if ( $this->current_element() === null )
        {
            $this->error("Parsing failed");
            return null;
        }
        
        $this->flush_elements();
        
        return $this->current_element()->process();
    }
}