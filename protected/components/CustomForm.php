<?php

class CustomForm extends CActiveForm
{
    public $model = null;
    public $auto_notes = true; ///< Display errors and messages on form begin
    
    function __construct(CBaseController $owner=NULL)
    {
        parent::__construct($owner);
        $this->enableClientValidation = true;
        $this->clientOptions = array(
            'validateOnSubmit'=>true,
            'errorCssClass' => 'errorField',
            'successCssClass' => '',
        );
        $this->errorMessageCssClass = 'error';
    }
    
    function init()
    {
        parent::init();

        if ( $this->auto_notes )
        {
            echo "<div class='message'>Fields marked with ".
                "<span class='required'>*</span> are required</div>\n";
                
            if ( $this->model != null )
            {
                 echo $this->errorSummary($this->model)."\n";
            }
        }
        
    }
    
    function beginRow($model,$attribute,$htmlOptions=array())
    {
        $css = array('row');
        if ( $model->hasErrors($attribute) )
            $css []= CHtml::$errorCss;
        if ( isset($htmlOptions['class']) )
            $css []= $htmlOptions['class'];
        $htmlOptions['class'] = implode(' ',$css);
        return CHtml::openTag('div',$htmlOptions);
    }
    
    function autoBeginRow($attribute,$htmlOptions=array())
    {
        return $this->beginRow($this->model,$attribute,$htmlOptions);
    }
    
    
    function endRow()
    {
        return CHtml::closeTag('div');
    }
    
    
    function textAreaRow($model,$attribute,$htmlOptions=array())
    {
        
		return  $this->beginRow($model,$attribute)              .
                $this->labelEx($model,$attribute)  .
                $this->textArea($model,$attribute,$htmlOptions).
                $this->error($model,$attribute)    .
                $this->endRow();
    }
    
    function autoTextAreaRow($attribute,$htmlOptions=array())
    {
        return $this->textAreaRow($this->model,$attribute,$htmlOptions);
    }
    
    function textRow($model,$attribute,$htmlOptions=array())
    {
        
		return  $this->beginRow($model,$attribute)              .
                $this->labelEx($model,$attribute)  .
                $this->textField($model,$attribute,$htmlOptions).
                $this->error($model,$attribute)    .
                $this->endRow();
    }
    
    function autoTextRow($attribute,$htmlOptions=array())
    {
        return $this->textRow($this->model,$attribute,$htmlOptions);
    }
    
    function passwordRow($model,$attribute)
    {
		return  $this->beginRow($model,$attribute)              .
                $this->labelEx($model,$attribute)  .
                $this->passwordField($model,$attribute).
                $this->error($model,$attribute)    .
                $this->endRow();
    }
    
    function autoPasswordRow($attribute)
    {
        return $this->passwordRow($this->model,$attribute);
    }
    
    function checkBoxRow($model,$attribute)
    {
		return  $this->beginRow($model,$attribute,
                        array('class'=>'checkbox'))      .
                $this->checkBox($model,$attribute) .
                $this->labelEx($model,$attribute)  .
                $this->error($model,$attribute)    .
                $this->endRow();
    }
    function autoCheckBoxRow($attribute)
    {
        return $this->checkBoxRow($this->model,$attribute);
    }
    
    function autoDropDownRow($attribute,$source_class,
                             $value_field,$text_field='name',
                             $criteria=null)
    {
		return  $this->beginRow($this->model,$attribute)              .
                $this->labelEx($this->model,$attribute)  .
                $this->dropDownList($this->model,$attribute,
                    $this->drop_down($source_class,$value_field,
                                     $text_field,$criteria) ).
                $this->error($this->model,$attribute)    .
                $this->endRow();
    }
    
    
    function autoCheckBoxListRow($attribute,$source_class,
                             $value_field,$text_field='name',
                             $criteria=null)
    {
		return  $this->beginRow($this->model,$attribute,
                            array('class'=>'checkbox-list')) .
                $this->labelEx($this->model,$attribute)  .
                $this->checkBoxList($this->model,$attribute,
                    $this->drop_down($source_class,$value_field,
                                     $text_field,$criteria),
                    array('template'=>
                          '<div class="checkbox">{input}{label}</div>',
                         'separator'=>''
                    )
                ).
                $this->error($this->model,$attribute)    .
                $this->endRow();
    }
    
    /**
     * @todo Change all the applicable stuff to calls to this
    */
    function autoRow($attribute,$input_html)
    {
		return  $this->beginRow($this->model,$attribute,
                            array('class'=>'checkbox-list')) .
                $this->labelEx($this->model,$attribute) .
                $input_html.
                $this->error($this->model,$attribute)    .
                $this->endRow();
    }
    
    static function drop_down($source_class,$value_field,$text_field='name',
                              $criteria=null)
    {
        if ( $criteria == null )
            $criteria = $source_class::model()->search()->criteria;
        return CHtml::listData(
            $source_class::model()->findAll($criteria),
            $value_field, $text_field );
    }
    
    protected function justAutocomplete($name,$complete_view,$value)
    {
        $id = CHtml::getIdByName($name);
        $url = is_array($complete_view) ? $complete_view : array($complete_view);
        return $this->widget('zii.widgets.jui.CJuiAutoComplete',
            array(
                'name'=>$id.'_autocomplete',
                'sourceUrl'=>$url,
                'options'=> array (
                    'select'=>"js:function(event, ui) {
                                $('#$id').val(ui.item.id);
                            }",
                ),
                'value'=>$value,
            ), true );
    }
    
    function plainAutoComplete($name,$complete_view,$id='',$value='')
    {
        return CHtml::hiddenField($name,$id,
                                  array('id'=>CHtml::getIdByName($name))).
                $this->justAutocomplete($name,$complete_view,$value);
    }
    
    function activeAutoComplete($model,$attribute,$complete_view,$def_val='')
    {
        $name = CHtml::activeName($model,$attribute);
        return $this->hiddenField($model,$attribute,
                                  array('id'=>CHtml::getIdByName($name))).
                $this->justAutocomplete($name,$complete_view,$def_val);
    }
    
    function autoCompleteRow($model,$attribute,$url,$def_val='')
    {
        
		return  $this->beginRow($model,$attribute)              .
                $this->labelEx($model,$attribute)  .
                $this->activeAutoComplete($model,$attribute,$url,$def_val).
                $this->error($model,$attribute)    .
                $this->endRow();
    }
    
    function autoAutoCompleteRow($attribute,$url,$def_val='')
    {
        return $this->autoCompleteRow($this->model,$attribute,$url,$def_val);
    }
    
    function submitRow($label='Submit',$htmlOptions=array())
    {
        return '<div class="row buttons">'.
            CHtml::submitButton($label,$htmlOptions).
           '</div>';
    }
    
}