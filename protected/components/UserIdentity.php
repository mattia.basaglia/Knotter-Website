<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;
    
    public function __construct($user='',$pass='')
    {
        $this->_id = -1;
        parent::__construct($user,$pass);
    }
	
    public function authenticate()
    {
        
        $user = User::model()->find('name=:username', array('username' => $this->username ) );
        
        
        if ( $user === null )
        {
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        }
        else if( !$user->valid_password($this->password))
        {
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        }
        else if ( !$user->can_login() )
        {
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        }
        else
        {
            $this->_id=$user->id_user;
            $this->username=$user->name;
            $this->errorCode=self::ERROR_NONE;

            $this->setState('name', $user->name);
            $this->setState('role', $user->id_role);
        }
        
        return $this->errorCode==self::ERROR_NONE;
        
    }
 
    public function getId()
    {
        return $this->_id;
    }
}