<?php

class GalleryRenderer extends Toolbox
{
    public $gallery = null;
    public $items = array();
    
    
    public $item_width = null;
    public $item_height = null;
    
    function init()
    {
        if ( $this->gallery != null )
        {
            $this->items = $this->gallery->items;
            if ( strlen($this->title) == 0 ) 
                $this->title = $this->gallery->name;
        }
        
        Controller::registerScriptFile(
            "fancybox/lib/jquery.mousewheel-3.0.6.pack.js"
        );
        Controller::registerScriptFile(
            "fancybox/source/jquery.fancybox.pack.js"
        );
        Controller::registerCssFile(
            "/fancybox/source/jquery.fancybox.css", '', 'js'
        );
        Yii::app()->clientScript->registerScript($this->id,
<<<JS
    $('#$this->id a').fancybox(
        {
            helpers : {
                title : { type : 'over' },
            }
        }
    );
JS
        );
        Yii::app()->clientScript->registerCss($this->id,
            "#$this->id img {".
            ($this->item_width==null?'':"width:{$this->item_width}px;").
            ($this->item_height==null?'':"height:{$this->item_height}px;}").
            "}"
        );
        
        parent::init();
        
        $this->renderItems();
        
    }
    
    function renderItems()
    {
        echo "<ul class='media-gallery' id='{$this->id}_items'>";
        foreach($this->items as $item)
        {
            echo "<li><a rel='$this->id'".
                " title='$item->description' href='".$item->resource_url()."' >".
                    $item->render_thumb().
                "</a></li>";
        }
        echo '</ul>';
    }
    
    function run()
    {
        
        parent::run();
    }
}