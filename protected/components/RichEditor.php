<?php

class RichEditor extends CInputWidget
{
    private function registerGlobalScript()
    {
        Controller::registerScriptFile("ckeditor/ckeditor.js");
        
        $gallery_options = array();
        $galls = Gallery::model()->findAll();
        foreach ( $galls as $gall )
            $gallery_options[]= array($gall->name,$gall->id_gallery);
        $gallery_options = json_encode($gallery_options);
        
        Yii::app()->clientScript->registerScript('ckeditor',
<<<JS

CKEDITOR.on( 'dialogDefinition', function( ev ) {
	if ( ev.data.name == 'GalleryDialog' )
	{
		var tab = ev.data.definition.getContents('main-tab');
        tab.get('id-gallery')['items'] = $gallery_options;
    }
});

JS
        );
    }
    
    private function registerSpecificScript()
    {
        list($name,$id)=$this->resolveNameID();

        $css_file = json_encode(array(
                        Controller::base_url()."/css/style.css",
                        Controller::base_url()."/css/toolbox.css"
                    ));
        $icon = Controller::base_url().'/img/icon/16x16/gallery.png';
        Yii::app()->clientScript->registerScript("ckeditor-$id",
<<<JS


var editor = CKEDITOR.replace('$id',{
    contentsCss: $css_file,
});

editor.on( 'pluginsLoaded', function( ev )
{
    editor.addCommand( 'GalleryCommand',
                    new CKEDITOR.dialogCommand( 'GalleryDialog' ) );

    editor.ui.addButton( 'GalleryButton',
        {
            label : 'Gallery',
            command : 'GalleryCommand',
            icon : '$icon',
        } );
});

JS
        );
    }
    
    function init()
    {
        $this->registerGlobalScript();
        list($name,$id)=$this->resolveNameID();
        $htmlOptions = $this->htmlOptions;
        $htmlOptions['id'] = $id;
        echo CHtml::textArea($name,$this->value,$htmlOptions);
        $this->registerSpecificScript();
    }
}