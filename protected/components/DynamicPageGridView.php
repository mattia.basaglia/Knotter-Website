<?php
YiiBase::import('zii.widgets.grid.CGridView');

class DynamicPageGridView extends CGridView
{
    public $page_size = 20;
    public $get_param = 'num';
    public $page_sizes = array(20,50,100,'infinity');
    public $dataProvider;
    
    

    function init()
    {
        if ( isset($_REQUEST[$this->get_param]) )
            $this->page_size = (int)$_REQUEST[$this->get_param];
            
        if ( $this->page_size <= 0 || !is_numeric($this->page_size) )
        {
            $this->page_size = 'infinity';
            $this->dataProvider->pagination = false;
        }
        else
            $this->dataProvider->pagination->pageSize = $this->page_size;
        
        $this->filterSelector = '{filter}, #'.$this->get_param;
        
        parent::init();

    }
    
    function renderSummary()
    {
        $page_sizes = $this->page_sizes;
        $page_sizes []= $this->page_size;
        $page_sizes = array_unique($page_sizes);
        natsort($page_sizes);
    
        echo '<div class="page-size">View '.
            CHtml::dropDownList($this->get_param,$this->page_size,
                                array_combine($page_sizes,$page_sizes)).
            ' items per page</div>';
          
        if(($count=$this->dataProvider->getItemCount())<=0)
        {
            echo '<div class="'.$this->summaryCssClass.'">No results found</div>';
        }
        else
            parent::renderSummary();
    }
}