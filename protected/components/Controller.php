<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
    
    public $tracking_js = 'piwikTracker.trackPageView();';
    
    protected $page_title;
    
    public $link_actions = array();
    
    function set_return_url($url=null)
    {
        if ( $url == null )
            $url = Yii::app()->request->url;
        else if ( is_array($url) && !isset($url[0]) )
            $url[0] = $this->id."/".$this->action->id;
        
        Yii::app()->user->returnUrl = $url;
    }
    
    static function base_url()
    {
        return Yii::app()->request->getBaseUrl(true);
    }
    
    function getPageTitle()
    {
        if ( isset($this->page_title) )
            return $this->page_title;
        $r = Yii::app()->name . " | " . ucfirst($this->id);
        if ( $this->action->id != 'index' )
            $r .= " | ".ucfirst($this->action->id);
        
        return $r;
    }
    
    function setPageTitle($pt)
    {
        $this->page_title = $pt;
    }
    
    function renderError()
    {
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('/main/error',$error);
		}
    }
    
    function registerScriptFile($file,$dir='js')
    {
        Yii::app()->clientScript->registerScriptFile(
            self::base_url()."/$dir/$file");
    }
    
    
    function registerCssFile($file,$media='',$dir='css')
    {
        Yii::app()->clientScript->registerCssFile(
            self::base_url()."/$dir/$file",$media);
    }
}