<?php

class WebUser extends CWebUser
{
    private $cache_role = null;
    private $cache_db_user = null;
    
    
    protected function afterLogin()
    {
        $this->cache_role = null;
        $this->cache_db_user = null;
    }
    
    protected function afterLogout()
    {
        $this->cache_role = null;
        $this->cache_db_user = null;
    }
    
    function role()
    {
        if ( $this->cache_role === null )
        {
            if ( $this->id == null )
                $this->cache_role =
                    UserRole::model()->findByAttributes(array('name'=>'guest'));
            else
                $this->cache_role = User::model()->findByPk($this->id)->role;
        }
        return $this->cache_role;
    }
    
    function has_role($role_name)
    {
        return $this->role()->has_role($role_name);
    }
    
    function db_user()
    {
        if ( $this->id == null )
            return null;
        
        if ( $this->cache_db_user === null )
        {
            $this->cache_db_user = User::model()->findByPk($this->id);
        }
        
        return $this->cache_db_user;
    }
    
    function url()
    {
        if ( $this->id == null )
            return null;
        return $this->db_user()->url();
    }
}