<?php

require_once(dirname(__FILE__)."/converter/HtmlConverter.php");
require_once(dirname(__FILE__)."/converter/Element_HTML_Filtered.php");
require_once(dirname(__FILE__)."/converter/Element_HTML_Post.php");
require_once(dirname(__FILE__)."/converter/Element_Markdown.php");
require_once(dirname(__FILE__)."/converter/MarkdownConverter.php");
require_once(dirname(__FILE__)."/converter/Element_Html_Database.php");

class MarkdownGalleryFilter implements MarkdownLineFilter
{
    
    function process_line($line,MarkdownConverter $converter)
    {
        $sub = array();
        if ( !preg_match('/^\[gallery:\s*([0-9]+)\s*\]\s*$/i',$line,$sub) )
        {
            return null;
        }
        
        $div = $converter->create_element('div');
        $div->attributes = array(
            'class'=>"toolbox gallery-stub",
            'contenteditable'=>"false",
            'data-gallery'=>$sub[1]
        );
        $gallery = Gallery::model()->findByPk($sub[1]);
        if ( $gallery != null )
            $div->push_html("<h3 class='title'>$gallery->name</h3>");
        else
            $div->push_html("<div class='error'>Unknown gallery</div>");
        
        return $div;
    }
}

class ConverterFacade
{
    static private $instance = null;
    /**
     * @return ConverterFacade Singleton instance
     */
    public static function instance()
    {
        if ( self::$instance == null )
            self::$instance = new ConverterFacade();
            
        return self::$instance;
    }
    /**
     * @return ConverterFacade Singleton instance
     */
    static function o()
    {
        return self::instance();
    }
    
    private $md_block_filters = array();
    
    function __construct()
    {
        Element_HTML_Filtered::$allowed_attributes['div'] []= 'data-gallery';
        Element_Markdown::$special_template['div'] = function($element) {
            if ( isset($element->attributes['data-gallery']) )
            {
                return "[Gallery:".$element->attributes['data-gallery']."]\n\n";
            }
            return null;
        };
        $this->md_block_filters = array( new MarkdownGalleryFilter() );
    }
    
    /**
     * Transform html into markdown
     */
    function html2md($html,&$errors=null)
    {
        $conv = new HtmlConverter;
        $conv->element_class = 'Element_Markdown';
        $result = $conv->convert($html);
        if ( $errors !== null )
            $errors = $conv->errors;
        return $result;
    }
    
    /**
     * Make HTML safe by removing unwanted elements or attributes
     */
    function user_html_filter($html,&$errors=null)
    {
        $conv = new HtmlConverter;
        $conv->element_class = 'Element_HTML_Filtered';
        $result = $conv->convert($html);
        if ( $errors !== null )
            $errors = $conv->errors;
        return $result;
    }
    
    /**
     * Make HTML database ready
     */
    function html2db ($html,&$errors=null)
    {
        $conv = new HtmlConverter;
        $conv->element_class = 'Element_Html_Database';
        $result = $conv->convert($html);
        if ( $errors !== null )
            $errors = $conv->errors;
        return $result;
    }
    
    /**
     * Make HTML ready for further filtering
     */
    function db2html ($html,&$errors=null)
    {
        $conv = new HtmlConverter;
        $conv->element_class = 'Element_HTML_Database_revert';
        $result = $conv->convert($html);
        if ( $errors !== null )
            $errors = $conv->errors;
        return $result;
    }
    
    
    
    /**
     * Apply custom filters to html input before rendering (eg: galleries)
     */
    function html_render($html,&$errors=null)
    {
        $conv = new HtmlConverter;
        $conv->element_class = 'Element_HTML_Post';
        $result = $conv->convert($html);
        if ( $errors !== null )
            $errors = $conv->errors;
        return $result;
    }
    
    /**
     * Transform markdown to html
     */
    function md2html($md,&$errors=null)
    {
        $conv = new MarkdownConverter;
        $conv->block_filters = $this->md_block_filters;
        $result = $conv->convert($md);
        if ( $errors !== null )
            $errors = $conv->errors;
        return $result;
    }
    
    /**
     * call md2html and html_render
     */
    function md_render($md,&$errors=null)
    {
        $r1 = $this->md2html($md,$errors_1);
        $r2 = $this->html_render($r1,$errors_2);
        if ( $errors !== null )
            $errors = array_merge($errors_1,$errors_2);
        return $r2;
    }
}