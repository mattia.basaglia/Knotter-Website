<?php

CHtml::$errorSummaryCss = 'error';
CHtml::$errorMessageCss= 'error';
CHtml::$errorCss= 'errorField';

require_once ( dirname(__FILE__).DIRECTORY_SEPARATOR.'base_config.php' );

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>$name,
    
    'defaultController' => 'main',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.controllers.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'knitting',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),

	// application components
	'components'=>array(
		'user'=>array(
            'class'=>'WebUser',
            'loginUrl'=>array('user/login'),
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
			'rules'=>array(
                
                // general
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				
                // tools
                'gii' => 'gii',
                
                // ticket
                'ticket/edit-post/<id:\d+>' => 'ticket/editPost',
                
                // page
                'page/edit/<page:.+>'=>'page/edit',
                'page/new/<page:.+>'=>'page/new',
                'page/new' => 'page/new',
                'page/edit-id/<id:\d+>'=>'page/editID',
                'page/<page:.+>'=>'page/view',
                ''=>'page/index',

                // main
                'css/<sheet:\w+>.css' => 'main/css',
                'error'=>'main/error',
                'search'=>'main/search',
                
                //user
                /*'login'=>'user/login',
                'logout'=>'user/logout',
                'register'=>'user/register',*/
                'user/view/<name:.+>' => 'user/view',
                
                
                // last resort
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                
			),
		),
		
		'db'=>$db,
		
		'errorHandler'=>array(
			// action to display errors
			'errorAction'=>'main/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
        
        
        'request'=>array(
            'enableCookieValidation'=>true,
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'description'=>$description,
	),
);