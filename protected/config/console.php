<?php

require_once ( dirname(__FILE__).DIRECTORY_SEPARATOR.'base_config.php' );

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Knotter',

	// preloading 'log' component
	'preload'=>array('log'),

	'import'=>array(
		'application.models.*',
    ),
    
	// application components
	'components'=>array(
		// uncomment the following to use a MySQL database
		'db'=>$db,
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
);