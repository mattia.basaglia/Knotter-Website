<?php

class MediaController extends Controller
{
    
    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + deleteItem, deleteGalery',
        );
    }
    
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('view','gallery'),
                'users'=>array('*'),
            ),
            array('allow',
                  'expression' => '$user->has_role("admin")',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
    function init()
    {
        
        if ( Yii::app()->user->has_role('admin') )
        {
            $this->link_actions = array(
                array('label'=>'View All',
                      'url'=>array('media/index') ),
                array('label'=>'Galleries',
                      'url'=>array('media/gallery') ),
                array('label'=>'Create Gallery',
                      'url'=>array('media/newGallery') ),
                array('label'=>'Create Item',
                      'url'=>array('media/create') ),
            );
        }
    }
    
    public function loadGallery($id)
    {
        $model=Gallery::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested gallery does not exist.');
        return $model;
    }
    
    function loadItem($id)
    {
        $model= MediaItem::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested item does not exist.');
        return $model;
    }
    
    function actionIndex()
    {
        $this->set_return_url();
        $this->render('index');
    }
    
    function actionGallery($id=null)
    {
        $this->set_return_url();
        
        if ( Yii::app()->user->has_role('admin') )
            $this->breadcrumbs ['Media']= array('media/index');
        else
            $this->breadcrumbs []= 'Media';
        
        if ( $id == null )
        {
            $this->breadcrumbs []= 'Gallery';
            $this->render('galleries');
        }
        else
        {
            $gallery = $this->loadGallery($id);
            
            if ( isset($_REQUEST['Gallery']['name']) &&
                Yii::app()->user->has_role('admin') )
            {
                $gallery->name = $_REQUEST['Gallery']['name'];
                $gallery->save();
                if ( !isset($_REQUEST['ajax']) )
                    $this->redirect(array('gallery','id'=>$gallery));
            }
            
            $this->render('gallery',array('gallery'=>$gallery));
        }
    }
    
    function actionView($id)
    {
        $this->set_return_url();
        $item = $this->loadItem($id);
        $this->render('item',array('item'=>$item));
    }
    
    function actionMove_up($gallery,$item)
    {
        $gi = GalleryItem::model()->findByAttributes(array(
            'id_gallery'=>$gallery, 'id_media_item'=>$item
        ));
        if ( $gi == null )
            throw new CHttpException(404,'Association not found');
        
        $upper = GalleryItem::model()->find( array(
            'condition' => 'id_gallery = :idg and `order` < :ord',
            'params' => array('idg'=>$gallery,'ord'=>$gi->order),
            'order'=>'`order` desc',
        ));
        
        if ( $upper != null )
        {
            $o = $gi->order;
            $gi->order = $upper->order;
            $upper->order = $o;
            $upper->save();
            $gi->save();
        }
        
        if ( !isset($_REQUEST['ajax']) )
            $this->redirect(array('gallery','id'=>$gallery));
    }
    
    function actionMove_down($gallery,$item)
    {
        $gi = GalleryItem::model()->findByAttributes(array(
            'id_gallery'=>$gallery, 'id_media_item'=>$item
        ));
        if ( $gi == null )
            throw new CHttpException(404,'Association not found');
        
        $upper = GalleryItem::model()->find( array(
            'condition' => 'id_gallery = :idg and `order` > :ord',
            'params' => array('idg'=>$gallery,'ord'=>$gi->order),
            'order'=>'`order` asc',
        ));
        
        if ( $upper != null )
        {
            $o = $gi->order;
            $gi->order = $upper->order;
            $upper->order = $o;
            $upper->save();
            $gi->save();
        }
        
        if ( !isset($_REQUEST['ajax']) )
            $this->redirect(array('gallery','id'=>$gallery));
    }
    
    function actionUnlink($gallery,$item)
    {
        $gi = GalleryItem::model()->findByAttributes(array(
            'id_gallery'=>$gallery, 'id_media_item'=>$item
        ));
        if ( $gi == null )
            throw new CHttpException(404,'Association not found');
        $gi->delete();
        
        if ( !isset($_REQUEST['ajax']) )
            $this->redirect(array('gallery','id'=>$gallery));
    }
    
    function actionLink($gallery,$item)
    {
        $this->loadGallery($gallery);
        $this->loadItem($item);
        if ( GalleryItem::model()->findByAttributes(array(
            'id_gallery'=>$gallery, 'id_media_item'=>$item )) != null )
            throw new CHttpException(400,'Item Exists');
        
        $model = new GalleryItem;
        $model->id_gallery = $gallery;
        $model->id_media_item = $item;
        $model->order = 1;
        
        $last = GalleryItem::model()->find(array(
            'condition'=>'id_gallery = :idg',
            'order'=>'`order` desc',
            'params'=>array('idg'=>$gallery)
        ));
        if ( $last != null )
            $model->order = $last->order + 1;
        $model->save(false);
        
        
        if ( !isset($_REQUEST['ajax']) )
            $this->redirect(array('gallery','id'=>$gallery));
    }
    
    function actionItem_autocomplete($term)
    {
        $matches = Yii::app()->db->createCommand()
            ->select('concat(title," - ",file) as value,
                      id_media_item as id')
            ->from('media_item')
            ->where('concat(title," - ",file) like :term',
                    array('term'=>"%$term%"))
            ->limit(10)
            ->order('id_media_item desc')
            ->queryAll();
        
        echo json_encode ( $matches );
    }
    
    function actionNewGallery()
    {
        $gall = new Gallery();
        $gall->name = "New Gallery";
        $gall->save();
        $this->redirect(array('gallery','id'=>$gall->id_gallery));
    }
    
    function actionCreate()
    {
        $this->render('choose_file');
    }
    
    function actionNewItem($file='')
    {
        $item = new MediaItem;
            
        if  ( isset($_POST['MediaItem']) )
        {
            $item->attributes = $_POST['MediaItem'];
            if ( $item->save() )
                $this->redirect($item->desc_url());
        }
        else
        {
            $item->file = $file;
            $info = pathinfo($file);
            $item->title =
                ucwords(preg_replace('/[-._]/',' ',$info['filename']));
            $item->mime_type = 'image/'.$info['extension'];
        }
        $this->render('new',array('model'=>$item));
    }
    
    function actionEditItem($id)
    {
        $item = $this->loadItem($id);
            
        if  ( isset($_POST['MediaItem']) )
        {
            $item->attributes = $_POST['MediaItem'];
            if ( $item->save() )
                $this->redirect($item->desc_url());
        }
        
        $this->render('new',array('model'=>$item));
    }
    
    function actionDeleteItem($id)
    {
        $this->loadItem($id)->delete();
        $this->redirect(array('index'));
    }
    
    function actionDeleteGallery($id)
    {
        $this->loadGallery($id)->delete();
        $this->redirect(array('index'));
    }
    
}