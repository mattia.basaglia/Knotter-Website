<?php

class MainController extends Controller
{
	public function actionIndex()
	{
        $this->redirect(array('page/index'));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
        $this->renderError();
	}

    function getPageTitle()
    {
        if ( isset($this->page_title) )
            return $this->page_title;
        
        return Yii::app()->name ." | ". ucfirst($this->action->id);
    }
    
    function actionCss($sheet)
    {
        header('Content-Type: text/css');
        $this->renderPartial('/css/'.$sheet); // todo: check minification performance
        //echo CssMin::minify($this->renderPartial('/css/'.$sheet,null,true));
    }
    
    
    function actionSearch()
    {
        $this->set_return_url($_REQUEST);
        $search = new SearchForm;
        if ( isset($_GET['q']) )
            $search->term = $_GET['q'];
        if ( isset($_REQUEST['SearchForm']) )
            $search->attributes = $_REQUEST['SearchForm'];
        $this->render('search',array('search'=>$search));
    }
    
    function actionTest()
    {
        $this->render('test');
    }
}