<?php

class PageController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }
    
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index','view'),
                'users'=>array('*'),
            ),
            array('allow',
                  'expression' => '$user->has_role("admin")',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
    function actionView($page)
    {
        $this->set_return_url();
        $post = Post::from_page($page,'page');
        
        if ( $post == null )
        {
            if ( Yii::app()->user->has_role("admin") )
                $this->redirect(array('page/new','page'=>$page));
            throw new CHttpException(404,"Page not found");
        }
        if ( $this->page_title == null )
            $this->pageTitle = Yii::app()->name . " | " . $post->title;
        
        if ( Yii::app()->user->has_role("admin") )
            $this->link_actions []= array(
                'label'=>'Edit Page', 'url'=>array('page/edit', 'page'=>$page)
            );
        
        $this->renderText($post->full_render(true));
    }
    
    function actionNew($page='')
    {
        $post = new Post;
        $post->page = $page;
        $this->do_edit($post);
    }
    
    function actionEdit($page)
    {
        $this->do_edit(Post::from_page($page,'page'));
    }
    function actionEditID($id)
    {
        $this->do_edit(Post::model()->findByPk($id));
    }
    
    protected function do_edit(Post $post)
    {
        if ( $post == null ||
                ( !$post->isNewRecord && $post->type->name != 'page' ) )
            throw new CHttpException(404,"Page not found");
        
        $edit = new Edit;
        $edit->id_user = Yii::app()->user->id;
        
        if ( isset($_POST['Edit']['text']) )
        {
            $edit->text = $_POST['Edit']['text'];
            if ( isset($_POST['Post']) )
                $post->attributes = $_POST['Post'];
            $post->id_type = PostType::findByName('page')->id_post_type;
            if ( $post->save() && $edit->validate('text') )
            {
                $edit->text = ConverterFacade::o()->html2db($edit->text);
                $edit->id_post = $post->id_post;
                $edit->save(false);
                $this->redirect(array('page/view','page'=>$post->page));
            }
        }
        else if ( !$post->isNewRecord )
            $edit->text = ConverterFacade::o()->db2html($post->content->text);
        
        $this->render('edit',array('post'=>$post,'edit'=>$edit));
    }
    
    function actionIndex()
    {
        $this->pageTitle=Yii::app()->name." | ".Yii::app()->params['description'];
        $this->actionView('index');
    }
    
    
    function actionCss($sheet)
    {
        header('Content-Type: text/css');
        $this->renderPartial('/css/'.$sheet); // todo: check minification performance
        //echo CssMin::minify($this->renderPartial('/css/'.$sheet,null,true));
    }
}

?>