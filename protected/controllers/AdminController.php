<?php

class AdminController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', 
        );
    }
    
    public function accessRules()
    {
        return array(
            array('allow',
                  'expression' => '$user->has_role("admin")',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
    public function actionIndex()
    {
        $this->render('index');
    }
    
    function actionPostHistory($id)
    {
        $post = Post::model()->findByPk($id);
        if ( $post == null )
            throw new CHttpException(404,"Post not found");
        $this->render('history',array('post'=>$post));
    }
}