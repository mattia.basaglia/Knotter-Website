<?php

class TicketController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index','view','project','autocomplete','error'),
                'users'=>array('*'),
            ),
            array('allow',
                  'actions'=>array('comment'),
                  'expression' => '$user->has_role("guest")',
            ),
            array('allow',
                  'actions'=>array('create','editPost'),
                  'expression' => '$user->has_role("user")',
            ),
            array('allow',
                  'actions'=>array('update','move'),
                  'expression' => '$user->has_role("contributor")',
            ),
            array('allow',
                  'expression' => '$user->has_role("admin")',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
    function init()
    {        
        parent::init();
        Yii::app()->errorHandler->errorAction=$this->id.'/error';
    }
    function actionError()
    {
        $this->breadcrumbs = array('Tickets'=>array('ticket/index'),'Error');
        $this->setDefaultActions();
        $this->renderError();
    }
    
    function setDefaultActions()
    {
        
        foreach(Project::model()->findAll() as $project )
            $this->link_actions []= array(
                'label'=>'New Ticket for '.$project->name,
                'url'=>array('ticket/create', 'project'=>$project->id_project));
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->set_return_url();
            
        $tick = $this->loadModel($id);
        
        $edit = $tick->post->do_request_reply('ticket');
        if ( $edit != null && !$edit->hasErrors() )
            $this->redirect(array('ticket/view','id'=>$id));        
        
        $this->render('view',array(
            'model'=>$tick,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($project)
    {
        $project = Project::model()->findByPk($project);
        if ( $project == null )
            throw new CHttpException(404,'The requested project does not exists');
        
        $model= new Ticket;
        $post = new Post;
        $edit = new Edit;

        $this->performAjaxValidation(array($model,$post,$edit));

        if(isset($_POST['Ticket']))
        {
            $model->attributes=$_POST['Ticket'];
            
            if(isset($_POST['Edit']['text']))
                $edit->text = $_POST['Edit']['text'];
                
            if(isset($_POST['Post']))
                $post->attributes = $_POST['Post'];
            
            $post->id_type = PostType::findByName('ticket')->id_post_type;
                
            if( $post->validate() && $edit->validate('text') && $model->validate() )
            {
                $post->save(false);
                $model->id_post = $post->id_post;
                $model->save(false);
                $edit->id_post = $post->id_post;
                $edit->id_user = Yii::app()->user->id;
                $edit->save(false);
                $this->redirect(array('view','id'=>$model->id_ticket));
            }
        }

        $this->render('create',array(
            'model'=>$model,
            'post' => $post,
            'edit' => $edit,
            'project' => $project,
        ));
    }

    protected function ticket_edit_text(Ticket$old,Ticket$new)
    {
        
        $text = "Updated ticket:\n";
        
        if ( $old->id_status != $new->id_status )
            $text .= "Status: {$old->status->name} -> {$new->status->name}\n";
        if ( $old->id_priority != $new->id_priority )
            $text .= "Status: {$old->priority->name} -> {$new->priority->name}\n";
        if ( $old->id_type != $new->id_type )
            $text .= "Status: {$old->type->name} -> {$new->type->name}\n";
        if ( $old->id_version != $new->id_version )
            $text .= "Version: ".
                $old->version->project->name." ".$old->version->name." -> ".
                $new->version->project->name." ".$new->version->name."\n";
        
        
        if ( $old->id_parent != $new->id_parent )
        {
            $text .= "Parent: ";
            if ( $old->id_parent != null )
                $text .= $old->parent->full_title();
            else
                $text .= "(none)";
            $text .= " -> ";
            if ( $new->id_parent != null )
                $text .=  $new->parent->full_title();
            else
                $text .= "(none)";
            $text .= "\n";
        }
        
        if ( $old->id_assignee != $new->id_assignee )
        {
            $text .= "Assigned to: ";
            if ( $old->id_assignee != null )
                $text .= $old->assignee->name;
            else
                $text .= "(nobody)";
            $text .= " -> ";
            if ( $new->id_assignee != null )
                $text .= $new->assignee->name;
            else
                $text .= "(nobody)";
            $text .= "\n";
        }
        
        if ( $old->commit != $new->commit )
        {
            $text .= "Commit: $old->commit -> $new->commit\n";
        }

        return $text;
    }
    
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        
        $post = $model->post;
        $edit = new Edit('autofill');

        $this->performAjaxValidation(array($model,$post));
        
        if(isset($_POST['Ticket']))
        {
            $model->attributes=$_POST['Ticket'];
            
            if(isset($_POST['Edit']))
                $edit->attributes = $_POST['Edit'];
                
            if(isset($_POST['Post']))
                $post->attributes = $_POST['Post'];
            
            $post->id_type = PostType::findByName('ticket')->id_post_type;
                
            if( $post->validate() && $model->validate() )
            {
                $edit->text =
                    $this->ticket_edit_text($this->loadModel($id),$model)."\n".
                    $edit->text;
                $post->save(false);
                $model->id_post = $post->id_post;
                $model->save(false);
                
                $post->do_reply($edit->text,Yii::app()->user,'ticket');
                
                $this->redirect(array('view','id'=>$model->id_ticket));
            }
        }

        $this->render('update',array(
            'model'=>$model,
            'edit'=>$edit,
            'post'=>$post,
        ));
    }

    function actionIndex()
    {
        $this->set_return_url();
        $this->setDefaultActions();
        $model=new Ticket('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Ticket']))
            $model->attributes=$_GET['Ticket'];

        $this->render('index',array(
            'model'=>$model,
        ));
    }

    public function actionProject($id)
    {
        $this->set_return_url();
        $project = Project::model()->findByPk($id);
        if ( $project == null )
            throw new CHttpException(404,'The requested project does not exists');
        
        $model=new Ticket('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Ticket']))
            $model->attributes=$_GET['Ticket'];

        $this->render('project',array(
            'model'=>$model,
            'project'=>$project,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Ticket the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Ticket::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested ticket does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Ticket $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='ticket-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    function actionAutocomplete($term)
    {
        $matches = Yii::app()->db->createCommand()
            ->select('concat(post.title," - #",ticket.id_ticket) as value,
                      ticket.id_ticket as id')
            ->from('ticket')
            ->join('post','post.id_post = ticket.id_post')
            ->where('concat(post.title," - #",cast(ticket.id_ticket as char))
                    like :term', array('term'=>"%$term%"))
            ->limit(10)
            ->order('id_ticket desc')
            ->queryAll();
        
        echo json_encode ( $matches );
    }
    
    function actionComment($id)
    {
        $post = Post::model()->findByPk($id);
        if($post===null)
            throw new CHttpException(404,'The requested comment does not exist.');
        if ( $post->type->name != $this->id )
            $this->redirect($post->url);

        while($post->parent != null )
            $post = $post->parent;
        
        if ( $post->ticket == null )
            throw new CHttpException(404,'The requested ticket does not exist.');
        
        $this->redirect(
            CHtml::normalizeUrl(array($this->id.'/view',
                                      'id'=>$post->ticket->id_ticket
                                )
            )."#post_$id"
        );
            
    }
    
    function actionEditPost($id)
    {
        
        $post = Post::model()->findByPk($id);
        if($post===null)
            throw new CHttpException(404,'The requested comment does not exist.');
        if ( $post->type->name != $this->id )
            $this->redirect($post->url);
        
        $post->edit_check();
        
        $top = $post;
        while($top->parent != null )
            $top = $top->parent;
        
        if ( $top->ticket == null )
            throw new CHttpException(404,'The requested ticket does not exist.');
        
        $edit = new Edit;
        $edit->text = $post->content->text;
        $edit->id_post = $id;
        $edit->id_user = Yii::app()->user->id;
        if ( isset($_POST['Edit']) )
        {
            $edit->text = $_POST['Edit']['text'];
            if ( strlen(trim($edit->text)) == 0 )
                $edit->addError('text',"You can't delete a comment");
            else if ( $edit->text == $post->content->text || $edit->save() )
            {
                $this->actionComment($id);
            }
        }
        
        $this->render('edit_post',array('post'=>$post,'top'=>$top,
                                        'edit'=>$edit));
    }
    
    function actionMove($id)
    {
        $ticket = $this->loadModel($id);
        if ( isset($_REQUEST['to']) )
        {
            $version = Version::model()->findByPk($_REQUEST['to']);
            if ( $version == null )
                $ticket->addError('id_version','Selected version does not exist');
            else
            {
                $ticket->id_version = $version->id_version;
                if ( $ticket->save(false) )
                {
                    $old = $this->loadModel($id);
                    $ticket->post->do_reply('Moved ticket from project '.
                                            $old->version->project->name);
                    $this->redirect(array('ticket/view','id'=>$id));
                }
            }
        }
        $this->render('move',array('model'=>$ticket));
    }
}