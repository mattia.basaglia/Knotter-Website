<?php

class UserController extends Controller
{
    

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
        if ( Yii::app()->request->urlReferrer == null ||
            Yii::app()->request->urlReferrer == Yii::app()->request->url )
            $this->redirect(Yii::app()->homeUrl);
        else
            $this->redirect( Yii::app()->request->urlReferrer );
	}
    
    function actionView($name)
    {
        $this->set_return_url();
        $user = User::model()->findByAttributes(array('name'=>$name));
        if ( $user == null )
            throw new CHttpException(404,"No user called '$name' was found");
        $this->render('user',array('user'=>$user));
    }
    
}