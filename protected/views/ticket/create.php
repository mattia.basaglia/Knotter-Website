<?php
/* @var $this TicketController */
/* @var $model Ticket */

$this->breadcrumbs=array(
	'Tickets'=>array('index'),
    $project->name =>
                array('project','id'=>$project->id_project),
    'Create'
);
?>

<h2 class="title">Create Ticket</h2>

<?php echo $this->renderPartial('_form', array(
            'model'=>$model,
            'post' => $post,
            'edit' => $edit,
            'project' => $project
        )); ?>