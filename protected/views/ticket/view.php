<?php
/* @var $this TicketController */
/* @var $model Ticket */

$this->breadcrumbs=array(
	'Tickets'=>array('index'),
    $model->version->project->name =>
                array('project','id'=>$model->version->id_project),
	$model->post->title,
);

$this->pageTitle = Yii::app()->name . " | Ticket | " . $model->post->title;


$this->link_actions []= array('label'=>'Create New Ticket',
                              'url'=>array('ticket/create',
                                    'project'=>$model->version->id_project));

?>

<h2 class="title"><?php
    echo $model->full_title();
?></h2>

<?php

if ( Yii::app()->user->has_role('contributor') )
{
    $edit_url = array('ticket/update', 'id'=>$model->id_ticket);
    $this->link_actions []= array(
        'label' => 'Update Ticket',
        'url' => $edit_url
    );
    
    $this->link_actions []= array(
        'label' => 'Move Ticket',
        'url' => array('ticket/move', 'id'=>$model->id_ticket)
    );
    
    echo '<div class="edit-post">'.CHtml::link('Update Ticket Status',$edit_url).'</div>';
}

$this->renderPartial('_details',array('model'=>$model));

$model->post->full_render();

foreach($model->post->children as $comment)
{
    $comment->full_render();
}

$model->post->reply_form('Add comment');

?>
