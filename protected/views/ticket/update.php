<?php
/* @var $this TicketController */
/* @var $model Ticket */

$this->breadcrumbs=array(
	'Tickets'=>array('index'),
    $model->version->project->name =>
                array('project','id'=>$model->version->id_project),
	$model->post->title=>array('view','id'=>$model->id_ticket),
	'Update',
);

$this->link_actions []= array('label'=>'Create New Ticket',
                              'url'=>array('ticket/create',
                                    'project'=>$model->version->id_project));

$this->link_actions []= array('label'=>'View Ticket',
                    'url'=>array('ticket/view','id'=>$model->id_ticket));

?>

<h2 class="title"><?php
    echo "Update Ticket ".$model->full_title();
?></h2>

<?php echo $this->renderPartial('_form', array(
            'model'=>$model,
            'post' => $post,
            'edit' => $edit,
            'project' => $model->version->project
        )); ?>