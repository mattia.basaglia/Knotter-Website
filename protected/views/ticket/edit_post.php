<?php
/** @var $form CustomForm */

$this->pageTitle = Yii::app()->name." | Ticket | Edit | ".$post->title;

$this->breadcrumbs = array(
    'Tickets' => array('ticket/index'),
    $top->title => array('ticket/view','id'=>$top->ticket->id_ticket),
    'Edit Comment'
);

$form = $this->beginWidget('CustomForm',array(
    'model'=>$edit,
    'method'=>'post',
    'id' => 'edit-post',
));

echo $form->autoTextAreaRow('text');

echo $form->submitRow('Save');

$this->endWidget();