<?php
/* @var $this TicketController */
/* @var $model Ticket */
/* @var $edit Edit */
/* @var $post Post */
/* @var $project Project */
/* @var $form CustomForm */
?>

<div class="form">
<?php
$form = $this->beginWidget('CustomForm', array(
	'id'=>'ticket-form',
    'model'=>$model
));

    echo $form->errorSummary($edit)."\n";
    echo $form->errorSummary($post)."\n";

    echo '<div class="main-form">';
    echo $form->textRow($post,'title');
    echo $form->textAreaRow($edit,'text');
    echo '</div>';

    echo '<div class="options-form" >';
    
    echo "<div class='row'>
            <label>Project</label>
            <span class='inactive'>$project->name</span>
        </div>";
    
    echo $form->autoDropDownRow('id_version','Version','id_version','name',
                array('condition'=>'id_project=:idp',
                      'params'=>array('idp'=>$project->id_project),
                      'order'=>'name desc',
                ) );
    echo $form->autoDropDownRow('id_type','TicketType', 'id_ticket_type');
    echo $form->autoDropDownRow('id_priority','TicketPriority',
                                'id_ticket_priority');
    echo $form->autoDropDownRow('id_status','TicketStatus','id_ticket_status');

    $avail_user=CHtml::listData(User::model()->findAll('id_role=1'),
                                'id_user','name');
    $avail_user['']='Not assigned';
    echo $form->beginRow($model,'id_assignee');
    echo $form->labelEx($model,'id_assignee');
    echo $form->dropDownList($model,'id_assignee',$avail_user);
    echo $form->error($model,'id_assignee');
    echo $form->endRow();

    echo $form->autoTextRow('commit',array('size'=>40,'maxlength'=>40));
    
    echo $form->autoAutoCompleteRow('id_parent','ticket/autocomplete',
            isset($model->parent) ? 
                $model->parent->full_title() :
                (
                    isset($_POST['Ticket_id_parent_autocomplete']) ?
                        $_POST['Ticket_id_parent_autocomplete'] :
                        ''
                )
        );
    
    echo '</div>';
    
    echo $form->submitRow($model->isNewRecord ? 'Create' : 'Save');

$this->endWidget();

?>

</div><!-- form -->