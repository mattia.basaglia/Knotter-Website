<?php
/* @var $this TicketController */
/* @var $model Ticket */
/* @var $form CustomForm */

$this->breadcrumbs=array(
	'Tickets'=>array('index'),
    $model->version->project->name =>
                array('project','id'=>$model->version->id_project),
	$model->post->title=>array('view','id'=>$model->id_ticket),
    'Move'
);

$this->pageTitle = Yii::app()->name . " | Ticket | " . $model->post->title;


$this->link_actions []= array('label'=>'Create New Ticket',
                              'url'=>array('ticket/create',
                                    'project'=>$model->version->id_project));

$this->link_actions []= array(
        'label' => 'View Ticket',
        'url' => array('ticket/view', 'id'=>$model->id_ticket)
    );

$this->link_actions []= array(
        'label' => 'Update Ticket',
        'url' => array('ticket/update', 'id'=>$model->id_ticket)
    );
?>

<h2 class="title">Move Ticket </h2>

<?php
$this->renderPartial('_details',array('model'=>$model));

$form = $this->beginWidget('CustomForm', array(
    'model'=>$model
));

$versions = Version::model()->findAll(array(
    'order'=>'id_project, name desc',
    'condition' => 'id_project != :idp',
    'params' => array('idp'=>$model->version->id_project),
));
$droplist = array();
foreach ( $versions as $v )
{
    $droplist[$v->id_version] = $v->project->name." ".$v->name;
}
echo CHtml::label('New Project/Version:','to',array('class'=>'inline')).' ';
echo CHtml::dropDownList('to',$model->id_version,$droplist).' ';
echo CHtml::submitButton('Move');
$this->endWidget();