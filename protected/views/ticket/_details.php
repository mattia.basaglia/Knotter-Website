<?php


function children_links(Ticket $model)
{
    $l = array();
    foreach($model->children as $c)
    {
        $l []= CHtml::link($c->full_title(),
                            array('ticket/view','id'=>$c->id_ticket));
    }
    return implode(", ",$l);
}



$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'version.project.name:text:Project',
		'version.name:text:Version',
		'type.name:text:Type',
		'priority.name:text:Priority',
		'status.name:text:Status',
        array(
            'label'=>'Parent',
            'type'=>'html',
            'value'=>!isset($model->parent) ? null :
                CHtml::link($model->parent->full_title(),
                            array('ticket/view','id'=>$model->id_parent) )
        ),
        array(
            'label'=>'Children',
            'type'=>'html',
            'value'=>empty($model->children) ? null : children_links($model) 
        ),
		'assignee.name:text:Assigned to',
		'commit',
	),
));