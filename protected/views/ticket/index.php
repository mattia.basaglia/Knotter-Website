<?php
/* @var $this TicketController */
/* @var $model Ticket */

$this->breadcrumbs = array (
    'Tickets'
);
    
?>

<h2 class="title">Tickets</h2>

<?php

$this->widget('DynamicPageGridView', array(
    'id'=>'ticket-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'cssFile'=>CHtml::normalizeUrl(array('main/css','sheet'=>'gridview')),
    'columns'=>array(
        array(
            'class' => 'CDataColumn',
            'name' => 'searchTitle',
            'type'=>'html',
            'value' => 'CHtml::link($data->post->title,
                                    array("ticket/view","id"=>$data->id_ticket))',
        ),
        array(
            'class'=>'CDataColumn',
            'name'=>'searchProject',
            'type'=>'html',
            'filter'=>CustomForm::drop_down('Project','id_project'),
            'value'=>'CHtml::link($data->version->project->name,
                    array("ticket/project","id"=>$data->version->id_project))',
        ),
        array(
            'class'=>'CDataColumn',
            'name'=>'id_type',
            'filter'=>CustomForm::drop_down('TicketType','id_ticket_type'),
            'value'=>'$data->type->name',
            'cssClassExpression'=>'"ticket-".$data->type->name',
        ),
        array(
            'class'=>'CDataColumn',
            'name'=>'id_priority',
            'filter'=>CustomForm::drop_down('TicketPriority','id_ticket_priority'),
            'value'=>'$data->priority->name',
            'cssClassExpression'=>'"ticket-".$data->priority->name',
        ),
        array(
            'class'=>'CDataColumn',
            'name'=>'id_status',
            'filter'=>CustomForm::drop_down('TicketStatus','id_ticket_status'),
            'value'=>'$data->status->name',
            'cssClassExpression'=>'"ticket-".$data->status->name',
        ),
        'assignee.name:text:Assigned to',
    ),
));

?>
