<?php
/* @var $this TicketController */
/* @var $model Ticket */


$this->breadcrumbs = array (
    'Tickets'=>array('index'),
    $project->name
);

$this->pageTitle = Yii::app()->name." | Tickets for ".$project->name;

$this->link_actions []= array('label'=>'Create Ticket',
                              'url'=>array('ticket/create',
                                           'project'=>$project->id_project));
    
?>

<h2 class="title">Tickets</h2>

<?php $this->widget('DynamicPageGridView', array(
    'id'=>'ticket-grid',
    'dataProvider'=>$model->search($project->id_project),
    'filter'=>$model,
    'cssFile'=>CHtml::normalizeUrl(array('main/css','sheet'=>'gridview')),
    'columns'=>array(
        array(
            'class' => 'CDataColumn',
            'name' => 'searchTitle',
            'type'=>'html',
            'value' => 'CHtml::link($data->post->title,
                                    array("ticket/view","id"=>$data->id_ticket))',
        ),
        array(
            'class'=>'CDataColumn',
            'header'=>'Project',
            'value'=>'$data->version->project->name',
        ),
        array(
            'class'=>'CDataColumn',
            'name'=>'id_version',
            'filter'=>CustomForm::drop_down('Version','id_version','name',
                        array('condition'=>'id_project=:idp',
                                  'params'=>array('idp'=>$project->id_project),
                                  'order'=>'name desc',
                            )
                        ),
            'value'=>'$data->version->name',
        ),
        array(
            'class'=>'CDataColumn',
            'name'=>'id_type',
            'filter'=>CustomForm::drop_down('TicketType','id_ticket_type'),
            'value'=>'$data->type->name',
            'cssClassExpression'=>'"ticket-".$data->type->name',
        ),
        array(
            'class'=>'CDataColumn',
            'name'=>'id_priority',
            'filter'=>CustomForm::drop_down('TicketPriority','id_ticket_priority'),
            'value'=>'$data->priority->name',
            'cssClassExpression'=>'"ticket-".$data->priority->name',
        ),
        array(
            'class'=>'CDataColumn',
            'name'=>'id_status',
            'filter'=>CustomForm::drop_down('TicketStatus','id_ticket_status'),
            'value'=>'$data->status->name',
            'cssClassExpression'=>'"ticket-".$data->status->name',
        ),
        'assignee.name:text:Assigned to',
    ),
)); ?>