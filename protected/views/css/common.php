
/* global style */
@font-face {
    font-family: "Dejavu Sans";
    src: url("<?php echo Controller::base_url();?>/font/DejaVuSans.ttf");
}
@font-face {
    font-family: "Droid Sans";
    src: url("<?php echo Controller::base_url();?>/font/DroidSans.ttf");
}
@font-face {
    font-family: "Droid Sans";
    src: url("<?php echo Controller::base_url();?>/font/DroidSans-Bold.ttf");
    font-weight: bold;
}
body
{
    color: black;
    background-color:white;
    font-family: Droid Sans, sans-serif;
}

img {
    max-height: 100%;
    max-width: 100%;
}


/* title */
.logo
{
    float: left;
    margin-right: 0.5em;
}

.title {
    margin: 0.25em;
}
header .title
{
    margin: 0 0 0.25em 0;
}
.title a, .title a:hover, .title a:active, .title a:link
{
    text-decoration: none;
    color: black;
}
.description
{
    font-style: oblique;
}


/* footer */
footer {
    border-top: 1px dotted #ddd;
    font-size: x-small;
    margin-top: 1em;
    padding-top: 0.5em;
    text-align: center;
}

/* grid view */

.grid-view .summary {
    font-size: x-small;
    font-style: oblique;
}

.grid-view {
    padding-top: 0;
}

