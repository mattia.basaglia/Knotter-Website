/*<?php
    echo '*'."/\n";
    $this->renderPartial('/css/common');
    echo "\n/*";
?>*/


/* global style */

body
{
    background: url("<?php echo $this->base_url(); ?>/img/webback.png") repeat-y scroll 0 0 white;
    margin: 0;
    padding: 0;
}

a, a:hover, a:active, a:link
{
    text-decoration: underline;
    color: #28C;
}

table {
    border-collapse: collapse;
}
td, th {
    border: 1px solid black;
}


/* title <?php
//see also common.php
?> */

.title-wrapper
{
    margin: .25em 0 .5em 3em;
    min-height: 80px;
}

.title a, .title a:hover, .title a:active, .title a:link
{
    text-decoration: none;
    color: black;
}


/* menu */

nav.menu
{
    background-color: #223;
    background-image:-webkit-linear-gradient(#445, #223, #112, #001, #223);
    background-image:   -moz-linear-gradient(#445, #223, #112, #001, #223);
    background-image:    -ms-linear-gradient(#445, #223, #112, #001, #223);
    background-image:     -o-linear-gradient(#445, #223, #112, #001, #223);
    background-image:        linear-gradient(#445, #223, #112, #001, #223);
    color: white;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
    border-radius: 1.5em;
}

header .menu ul
{
    list-style: none;
    padding: 0 2em;
    margin: 0;
    display: inline-block;
    font-family: Dejavu Sans, sans-serif;
}
header .menu li
{
    display: inline-block;
    font-size: small;
    font-weight: bold;
}
header .menu a:active, header .menu a:link, header .menu a:hover
{
    text-decoration: none;
    display: inline-block;
    padding: 1em;
}
.menu li.active, .menu li:hover
{
    text-decoration: none;
    background: white;
    color: black;
}

/*breadcrumbs*/

nav.breadcrumbs {
    min-height: 0.5em;
}
div.breadcrumbs {
    background-color: rgba(200, 200, 200, 0.3);
    border-radius: 0 0 10px 10px;
    font-size: small;
    margin: 0 2em;
    padding: 0.5em 1em;
}

/*Page structure*/
article {
    padding-left: 2em;
}


#content
{
    display: inline-block;
    vertical-align: top;
    width: 75%;
    min-height: 15em;
}


#page
{
    padding: 0 1em;
}


/* sidebar */
#sidebar
{
    border-left: 1px dotted #DDD;
    display: inline-block;
    margin: 1em;
    width: 15%;
    padding-left: 1em;
    /*position: absolute;*/
}
#sidebar .title {
    color: #222;
    font-size: small;
    font-weight: bold;
    margin: 1em 0 0;
}
#sidebar ul {
    font-size: small;
    margin: 0;
    padding: 0;
}
#sidebar li {
    list-style: disc inside none;
}
#sidebar aside
{
    padding-left: 1em;
}
#sidebar .search
{
    padding-left: 0;
}
#sidebar .avatar {
    float: right;
}

.invisible, #sidebar .search-text
{
    display: none !important;
}


/* form */
span.required {
    color: red;
    font-size: small;
    display: inline-block;
    vertical-align: top;
}

div.errorField
{
    border: 1px solid #e55;
    padding: 0.5em;
}

.login {
    padding-left: 5px;
}
form label {
    display: block;
}

.checkbox label, label.inline {
    display: inline;
}

.checkbox-list .checkbox {
    display: inline-block;
    margin: 0 0.5em;
}

form .row {
    margin: 1em 0;
}

textarea {
    min-height: 10em;
    resize: vertical;
    width: 100%;
}

input { vertical-align: middle; }

input, textarea, .inactive
{
    border: 1px solid #DDD;
    border-radius: 5px;
    padding: 0.25em;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
}
input[type="text"], input[type="password"], textarea
{
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1) inset;
}

input[type="submit"]
{
    background:-webkit-linear-gradient(#eeeeee, #dedede, #dddddd, #efefef, #eeeeee) repeat scroll 0 0 transparent;
    background:   -moz-linear-gradient(#eeeeee, #dedede, #dddddd, #efefef, #eeeeee) repeat scroll 0 0 transparent;
    background:    -ms-linear-gradient(#eeeeee, #dedede, #dddddd, #efefef, #eeeeee) repeat scroll 0 0 transparent;
    background:     -o-linear-gradient(#eeeeee, #dedede, #dddddd, #efefef, #eeeeee) repeat scroll 0 0 transparent;
    background:        linear-gradient(#eeeeee, #dedede, #dddddd, #efefef, #eeeeee) repeat scroll 0 0 transparent;
    color: black;
    cursor:pointer;
}

.inactive
{
    color: #777;
    display: inline-block;
}


.options-form {
    display: inline-block;
    width: 40%;
}
.options-form label {
    float: left;
    width: 30%;
    text-align: right;
    padding-right: 1em;
}
.options-form input, .options-form select, .options-form .inactive {
    width: 60%;
}

.main-form {
    display: inline-block;
    margin-right: 2%;
    vertical-align: top;
    width: 58%;
}

.result-content
{
    display:none;
}
.toggle-button
{
    font-size: small;
    cursor:pointer;
}

/* messages */
div.message, div.success, div.error {
    color: #333;
    padding: 5px;
    margin: 1em;
}

div.message {
    background-color: #ffe;
    border: 1px solid #ee5;
}

div.success {
    background-color: #efe;
    border: 1px solid #5e5;
}

div.error {
    background-color: #fee;
    border: 1px solid #e55;
}


/* Post */
.user {
    display: inline-block;
    margin: 0 0 0 1em;
    vertical-align: top;
}

.avatar-large{
    float: left;
    margin: 0 1em 0 0.5em;
}

.ticket {
    border: 1px solid #CCCCCC;
    border-radius: 10px 10px 10px 10px;
    margin: 0.5em;
    padding: 0.5em;
    background: white;
}

.ticket .post-info {
    border-bottom: 1px solid #EEEEEE;
    padding-bottom: 0.25em;
}


.post-info > img {
    vertical-align: middle;
}

.ticket .post {
    margin-top: 0.5em;
}


.timestamp {
    float: right;
    font-size: x-small;
    font-weight: normal;
}


.user-name {
    display: inline-block;
    vertical-align: middle;
}

.user-role {
    font-size: small;
    text-align: right;
}


.user-name > a {
    font-weight: bold;
}

.user-name > a:link {
    text-decoration: none;
}


.page .timestamp {
    float: none;
}

.page .post-info {
    float: right;
}

.edit-post {
    text-align: right;
}
.edit-post > a {
    font-size: small;
    text-decoration: none;
}
.edit-post > a:hover { text-decoration: underline; }