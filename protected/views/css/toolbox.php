.media-gallery li {
    display: inline-block;
    margin: 0.25em;
}

.media-gallery img {
    box-shadow: 2px 2px 5px black;
}

.toolbox {
    background: rgba(0, 0, 0, 0.8);
    border: 3px ridge white;
    border-radius: 10px;
    box-shadow: 8px 8px 24px gray;
    padding: 0.5em;
    color: white;
    margin:1em;
}
.toolbox > .title { cursor: pointer; }
.toolbox > a, .toolbox > a:link {
    float: right;
    font-size: small;
    text-decoration: none;
    font-weight: bold;
}
.toolbox a, .toolbox a:hover, .toolbox a:link { color: white; }


.tool-buttons li {
    background-color: #555;
    border: 2px outset #ddd;
    border-radius: 10px;
    display: inline-block;
    margin: 0.1em;
    padding: 0.4em;
}

.tool-buttons li:hover {
    background-color: #777;
    border-color: white;
    padding: 0.5em;
    margin: 0;
}

.toolbox.gallery-stub > .title { cursor: auto; }