
/* custom cells */
.ticket-critical {
    background-color: rgba(255, 0, 0, 0.5);
}
.ticket-major {
    background-color: rgba(255, 128, 0, 0.5);
}
.ticket-normal {
    background-color: rgba(255, 255, 0, 0.5);
}
.ticket-minor {
    background-color: rgba(0, 255, 0, 0.5);
}

.ticket-new {
    background-color: rgba(200, 0, 0, 0.7);
}
.ticket-open {
    background-color: rgba(255, 0, 0, 0.5);
}
.ticket-wip {
    background-color: rgba(0, 0, 128, 0.5);
}
.ticket-closed {
    background-color: rgba(0, 128, 0, 0.5);
}
.ticket-wontfix {
    background-color: rgba(128, 128, 128, 0.5);
}

.ticket-feature {
    background-color: rgba(0, 0, 255, 0.3);
}
.ticket-bug {
    background-color: rgba(255, 0, 0, 0.3);
}

/* style */
.grid-view table.items tr.odd { background-color: #fff; }
.grid-view table.items tr.even { background-color: #ddd; }
.grid-view .filters { background: #ddd; }

.grid-view table.items tr.odd:hover,
.grid-view table.items tr.even:hover {background-color: #bbe; }

.grid-view table.items tr.selected {background-color: #bbe; }

.grid-view table.items tr.selected:hover {background-color: #ccf; }

.grid-view table.items th
{
	text-align: center;
    color: black;
<?php
    $lg = "linear-gradient(#eef,#ddf,#ccf,#bbe,#ccf)";
    $browsers = array('-webkit-','-moz-','-ms-','-o-','');
    foreach($browsers as $b)
        echo "background: $b$lg repeat scroll 0 0 transparent;\n";
?>
}

.grid-view table.items th a { color: black; text-decoration: none; }
.grid-view table.items td a { color: #126; text-decoration: none; }
.grid-view table.items td a:hover { text-decoration: underline; }

.grid-view th:first-of-type { border-radius: 10px 0 0 0; }
.grid-view th:last-of-type { border-radius: 0 10px 0 0; }
.grid-view table.items .filters td { padding: 0; }

/*defaults*/

.grid-view { padding: 15px 0; }

.grid-view table.items
{
	border-collapse: separate;
    border-spacing: 1px;
	width: 100%;
    color: black;
}

.grid-view table.items th, .grid-view table.items td
{
	font-size: 0.9em;
	padding: 0.3em;
}

.grid-view table.items td, .grid-view table.items th
{
	border: none;
}


<?php $img_url = $this->base_url()."/img/gridview/"; ?>

.grid-view-loading { background:url(<?php echo$img_url;?>loading.gif) no-repeat; }

.grid-view table.items th a.sort-link
{
	background:url(<?php echo$img_url;?>unsorted.gif) right center no-repeat;
	padding-right: 10px;
}


.grid-view table.items th a.asc
{
	background:url(<?php echo$img_url;?>up.gif) right center no-repeat;
	padding-right: 10px;
}

.grid-view table.items th a.desc
{
	background:url(<?php echo$img_url;?>down.gif) right center no-repeat;
	padding-right: 10px;
}

.grid-view .summary
{
	margin: 0 0 5px 0;
	text-align: right;
}

.grid-view .pager
{
	margin: 5px 0 0 0;
	text-align: right;
}

.grid-view .empty
{
	font-style: italic;
}
.grid-view .filters { text-align: center; }
.grid-view .filters input,
.grid-view .filters select
{
	width: 100%;
}

.grid-view .filters input[type="text"]
{
	width: 90%;
    margin: 0.2em;
}

/*Pager & co*/
.page-size
{
    font-size: small;
    margin-bottom: 0;
    float: left;
    padding-left: 24px;
}

.pager{ font-size: small; }