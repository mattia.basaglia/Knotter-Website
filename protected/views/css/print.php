/*<?php
    echo '*'."/\n";
    $this->renderPartial('/css/common');
    echo "\n/*";
?>*/
a, a:hover, a:active, a:link {
    text-decoration: none;
    color: black;
}

body {
    color: black;
    font-family: sans-serif;
    background: white;
}

nav, #sidebar,
input, textarea, .inactive, form, select, .grid-view .filters,
.page .post-info, .post-info img, .post .timestamp,  .post-info .user-role
{display: none;}

footer { margin-top: 1em; }

table { border-collapse: collapse; }
td, th { border: 1px solid black !important; text-align: center; }


.logo { height: 48px; width: 48px; }
header .title { font-size: large; }
header .description { font-size: small; }
.title-wrapper { margin: 0 0 1em 3em; }



.post-info div {
    display: inline-block;
    font-size: small;
    font-style: italic;
    margin-right: 0.25em;
}
.post { padding-left: 1.5em; margin-bottom: 0.5em; }