<?php
/* @var $this PageController */
/* @var $edit Edit */
/* @var $post Post */
/* @var $form CustomForm */

$this->breadcrumbs = array();
if ( !$post->isNewRecord )
{
    $this->breadcrumbs [$post->title] =
                                 array('page/view','page'=>$post->page);
}
$this->breadcrumbs []= 'Edit';

Controller::registerScriptFile("ckeditor/ckeditor.js");
$css_file = $this->base_url()."/css/style.css";

Yii::app()->clientScript->registerScript('ckeditor',
<<<JS


CKEDITOR.replace('Edit[text]',{
    contentsCss: '$css_file',
});
JS
);

$form = $this->beginWidget('CustomForm', array(
    'model'=>$post
));

echo $form->errorSummary($edit);
echo $form->autoTextRow('title');
echo $form->autoTextRow('page');
echo $form->textAreaRow($edit,'text');
echo $form->submitRow('Save');
$this->endWidget();
