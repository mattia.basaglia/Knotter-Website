<?php

$this->pageTitle = Yii::app()->name." | User |".$user->name;

$this->link_actions=array(
    array('label'=>'Content by '.$user->name,
        'url' => array('main/search','SearchForm[username]'=>$user->name)
    )
);

echo '<h2 class="title">'.CHtml::encode($user->name).'</h2>';

echo '<p>'.CHtml::encode($user->role->name).'</p>';
echo '<div class="user">';
echo '<img alt="" src="'.$user->avatar_url(128).'" class="avatar-large" />';
echo CHtml::encode($user->description);
echo '</div>';

if ( $user->role->has_role('contributor') )
{
    echo '<h3 class="title">Active tasks</h3>';
    
    
    $tick=new Ticket('search');
    $tick->unsetAttributes();  // clear any default values
    if(isset($_GET['Ticket']))
        $tick->attributes=$_GET['Ticket'];
        
    $dp = $tick->search();
    
    $dp->criteria->addSearchCondition('id_assignee',$user->id_user);
    $dp->criteria->addNotInCondition('status.name',array('wontfix','closed'));

    $this->widget('DynamicPageGridView', array(
    'id'=>'ticket-grid',
    'dataProvider'=>$dp,
    'filter'=>$tick,
    'cssFile'=>CHtml::normalizeUrl(array('main/css','sheet'=>'gridview')),
    'columns'=>array(
        array(
            'class' => 'CDataColumn',
            'name' => 'searchTitle',
            'type'=>'html',
            'value' => 'CHtml::link($data->post->title,
                                    array("ticket/view","id"=>$data->id_ticket))',
        ),
        array(
            'class'=>'CDataColumn',
            'name'=>'searchProject',
            'type'=>'html',
            'filter'=>CustomForm::drop_down('Project','id_project'),
            'value'=>'CHtml::link($data->version->project->name,
                    array("ticket/project","id"=>$data->version->id_project))',
        ),
        array(
            'class'=>'CDataColumn',
            'name'=>'id_type',
            'filter'=>CustomForm::drop_down('TicketType','id_ticket_type'),
            'value'=>'$data->type->name',
            'cssClassExpression'=>'"ticket-".$data->type->name',
        ),
        array(
            'class'=>'CDataColumn',
            'name'=>'id_priority',
            'filter'=>CustomForm::drop_down('TicketPriority','id_ticket_priority'),
            'value'=>'$data->priority->name',
            'cssClassExpression'=>'"ticket-".$data->priority->name',
        ),
        array(
            'class'=>'CDataColumn',
            'name'=>'id_status',
            'filter'=>CustomForm::drop_down('TicketStatus','id_ticket_status'),
            'value'=>'$data->status->name',
            'cssClassExpression'=>'"ticket-".$data->status->name',
        ),
    ),
));
}


$post=new Post('search');
$post->unsetAttributes();  // clear any default values
if(isset($_GET['Post']))
    $post->attributes=$_GET['Post'];
    
echo '<h3 class="title">Content by this user</h3>';
$this->widget('DynamicPageGridView', array(
    'id'=>'search-grid',
    'cssFile'=>CHtml::normalizeUrl(array('main/css','sheet'=>'gridview')),
    'dataProvider'=>$post->search_by_user($user),
    'filter' => $post,
    'get_param' => 'npost',
    'page_sizes' => array(8,20,50,100,'infinity'),
    'page_size' => 8,
    'columns' => array(
        array(
            'name'=>'title',
            'type'=>'html',
            'value'=>'$data->link()'
        ),
        array(
            'class'=>'CDataColumn',
            'name'=>'id_type',
            'header'=>'Type',
            'filter'=>CustomForm::drop_down('PostType','id_post_type'),
            'value'=>'$data->type->name',
        ),
        'content.date'
    )
));