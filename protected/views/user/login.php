<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CustomForm  */

$this->pageTitle = "Log In | ".Yii::app()->name;

?>

<h2 class="title">Login</h2>

<div class="login">
<?php

$form=$this->beginWidget('CustomForm', array(
	'id'=>'login-form',
    'model'=>$model
)); 

    echo $form->autoTextRow('username');
    echo $form->autoPasswordRow('password');
    echo $form->autoCheckBoxRow('rememberMe');
    

	echo '<div class="row buttons">'.
            CHtml::submitButton('Log In').
        '</div>';

$this->endWidget(); ?>

<p>Can't log in?</p>
<ul class="tml-action-links">
    <li>
    <a rel="nofollow" href="<?php
        echo $this->base_url();?>/register">Register</a>
    </li>
    <li>
        <a rel="nofollow" href="<?php
            echo $this->base_url();?>/lostPassword">Recover Lost Password</a>
    </li>
</ul>

</div><!-- form -->
