<?php

// ========= Users =========
$this->beginWidget('Toolbox',array(
    'title'=>'Users',
));

$this->endWidget();

// ========= Media =========
$this->beginWidget('Toolbox',array(
    'title'=>'Media',
));
$this->widget('zii.widgets.CMenu',array(
    'items'=>array(
        array('label'=>'View All',
              'url'=>array('media/index') ),
        array('label'=>'Galleries',
              'url'=>array('media/gallery') ),
        array('label'=>'Create Gallery',
              'url'=>array('media/newGallery') ),
        array('label'=>'Create Item',
              'url'=>array('media/create') ),
    ),
    'htmlOptions' => array('class' => 'tool-buttons'),
));
$this->endWidget();


// ========= Pages =========
$this->beginWidget('Toolbox',array(
    'title'=>'Pages',
));
$this->endWidget();


// ========= Projects =========
$this->beginWidget('Toolbox',array(
    'title'=>'Projects',
));
$this->endWidget();