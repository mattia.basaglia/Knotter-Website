<!DOCTYPE html >
<?php /* @var $this Controller */ ?>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
    <link rel='shortcut icon' href='<?php
            echo $this->base_url();
            ?>/img/favicon.png' type='image/png' />
    <?php
        $this->registerCssFile("style.css", 'screen, projection');
        $this->registerCssFile("print.css", 'print');
    /*                                              
	<link rel="stylesheet" type="text/css" href="<?php
            echo $this->base_url();
            ?>/css/style.css" media="" />*/
    ?>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">
    
    <header role="banner">
        <div class="title-wrapper">
            <?php echo CHtml::link(
                '<img class="logo" src="'.$this->base_url().'/img/icon/scalable/logo-big.svg" '.
                'alt="('.CHtml::encode(Yii::app()->name).' logo)" width="80" />',
                array('/main/index')
            ); ?>
            <h1 class="title"><?php
                echo CHtml::link(Yii::app()->name,array('/main/index'));
            ?></h1>
        <div class="description"><?php
            echo Yii::app()->params['description']; ?> </div>
        </div>
        
        <nav class="menu" role="navigation">
                <a class="invisible" href="#content">Skip to content</a>
                <div class="menu-main-container">
<?php $this->widget('zii.widgets.CMenu',array(
    'items'=>array(
        array('label'=>'Home', 'url'=>array('page/index')),
        array('label'=>'Showcase', 'url'=>array('page/showcase'),
              'active'=>($this->id=='page'&&isset($_GET['page'])&&
                         $_GET['page']=='showcase') ),
        array('label'=>'Documentation', 'url'=>array('wiki/index')),
        array('label'=>'Forum', 'url'=>array('forum/index')),
        array('label'=>'Get Involved', 'url'=>array('page/get-involved'),
              'active'=>($this->id=='page'&&isset($_GET['page'])&&
                         $_GET['page']=='get-involved') ),
        array('label'=>'Download', 'url'=>array('release/latest')),
    ),
    'htmlOptions' => array('class' => 'menu'),
)); ?> 
                </div>
        </nav>
        <nav class="breadcrumbs" role="navigation">
            <?php
                if ( isset($this->breadcrumbs) )
                {
                    $this->widget('zii.widgets.CBreadcrumbs', array(
                        'links'=>$this->breadcrumbs,
                    ));
                }
            ?>
        </nav>
    </header>
    
    
    <div id="content">
        <article>
            <?php echo $content; ?>
        </article>
    </div><!--content-->

	<div id="sidebar" role="complementary">
        <aside id="search" class="search">
            <form method="get" action="<?php
                echo CHtml::normalizeUrl(array('main/search')); ?>">
                <label for="term" class="search-text">Search</label>
                <input type="text" name="q" id="term" placeholder="Search" value="" />
                <input type="submit" class="search-text" value="Search" />
            </form>
        </aside>
        
        <aside>
        <?php
            echo '<h3 class="title">'.Yii::app()->user->name.'</h3>';
            if ( !Yii::app()->user->isGuest )
            {
                echo '<img alt="" class="avatar" src="'.
                        Yii::app()->user->db_user()->avatar_url(48).'" />';
            }
        ?>
        <?php $this->widget('zii.widgets.CMenu',array(
            'items'=>array(
                array('label'=>'View Profile',
                      'url'=>Yii::app()->user->url(),
                      'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Site Admin', 'url'=>array('admin/index'),
                      'visible'=>Yii::app()->user->has_role('admin')),
                
                array('label'=>'Login', 'url'=>array('user/login'),
                      'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Register', 'url'=>array('user/register'),
                      'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Logout',
                      'url'=>array('user/logout'),
                      'visible'=>!Yii::app()->user->isGuest),
                
            ),
            'htmlOptions' => array('class' => 'menu'),
        )); ?>
        </aside>
        
        <?php
            if ( !empty($this->link_actions) )
            {
                echo '<aside><h3 class="title">Actions</h3>';
                 $this->widget('zii.widgets.CMenu',array(
                    'items'=>$this->link_actions,
                    'htmlOptions' => array('class' => 'menu'),
                ));
                echo '</aside>';
            }
        ?>
        
        <aside>
        <h3 class="title">Quick Links</h3>
        <?php $this->widget('zii.widgets.CMenu',array(
            'items'=>array(
                array('label'=>'Search',
                      'url'=>array('main/search') ),
                array('label'=>'Forum',
                      'url'=>array('forum/index') ),
                array('label'=>'Tickets',
                      'url'=>array('ticket/index') ),
                array('label'=>'Wiki',
                      'url'=>array('wiki/index') ),
            ),
            'htmlOptions' => array('class' => 'menu'),
        )); ?>
        </aside>
            
        <aside>
        <h3 class="title">News</h3>
        <?php
        
        $nit = array ( array('label'=>'News Feed', 'url'=>array('news/feed') ) );
        $nid = PostType::findByName('news')->id_post_type;
        $newt = Post::model()->with('content')->findAll(array(
                'condition' => "id_type = $nid",
                'order' => 'content.date desc',
                'limit' => 5
        ));
        foreach($newt as $n)
        {
            $nit []= array('label'=>$n->title,
                           'url'=>array('news/view','id'=>$n->id_post)
                        );
        }
        
        $this->widget('zii.widgets.CMenu',array(
            'items'=>$nit,
            'htmlOptions' => array('class' => 'menu'),
        ));
        
        ?>
        </aside>
        
    </div>

	<footer role="banner">
		Copyright &copy; 2012 - <?php echo date('Y'); ?> Mattia Basaglia - 
		All Rights Reserved
	</footer><!-- footer -->

</div><!-- page -->

<?php
$pkbaseurl = $this->base_url().'/piwik';
?>
<!--Piwik Analytics-->
<script type='text/javascript' src='<?php echo$pkbaseurl?>/piwik.js' ></script>
<script type='text/javascript'>
try {
    var piwikTracker = Piwik.getTracker('<?php echo$pkbaseurl?>/piwik.php', 1);
    <?php echo$this->tracking_js;?>
    piwikTracker.enableLinkTracking();
} catch( err ) {}
</script>
<noscript><img src="<?php echo $pkbaseurl;?>/piwik.php?idsite=1&rec=1" alt="" /></noscript>
<!--End Piwik Analytics-->
</body>
</html>
