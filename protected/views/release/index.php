<?php
// todo remove?
$this->pageTitle = Yii::app()->name." | Release history";
?>
<h2  class="title">Release history</h2>
<?php

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'release-grid',
	'dataProvider'=>Version::model()->search(),
    'enablePagination'=>false,
    'cssFile'=>CHtml::normalizeUrl(array('main/css','sheet'=>'gridview')),
	'columns'=>array(
        'project.name:text:Project',
		'name:text:Version',
        array(
            'class'=>'CDataColumn',
            'header'=>'Release notes',
            'type'=>'html',
            'value'=>'isset($data->releaseNotes) ?
                        $data->releaseNotes->link() :
                        "Not yet released"',
        ),
    )
) );
