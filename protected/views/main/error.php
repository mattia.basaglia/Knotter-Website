<?php
/* $code $message $type $file $line $trace $source */

$this->pageTitle=Yii::app()->name . ' | Error';

$this->tracking_js =
<<<JS
piwikTracker.setDocumentTitle('$code/URL = '+String(document.location.pathname+document.location.search).replace(/\//g,"%2f") + '/From = ' + String(document.referrer).replace(/\//g,"%2f"));
JS
. " " . $this->tracking_js;
?>

<h2 class="title">Error <?php echo $code; ?></h2>

<div class="error">
<?php echo CHtml::encode($message); ?>
</div>

<?php

if ( Yii::app()->user->has_role('developer') )
{
    Yii::app()->clientScript->registerCss('detail-report',
<<<CSS
    .trace
    {
        font-family: monospace;
        width: 100%;
        font-size:small;
    }
    .trace th
    {
        border: none;
        border-bottom: 1px solid black;
    }
    .trace td:first-child
    {
        border: none;
        white-space: pre;
    }
    .trace td
    {
        border: none;
        border-left: 1px solid black;
        padding-left: 0.5em;
    }
    .linenum
    {
        text-align: right;
        padding-right: 0.5em;
    }
    
    
    .odd {background-color:#ddd;}
    
CSS
    );
    
    $trace_lines = explode("\n",$trace);
    $traces = array();
    foreach($trace_lines as $tline)
    {
        $sub = array();
        if ( preg_match(";#[0-9]+\s+([^(]+)\(([0-9]+)\):(.*);",$tline,$sub) )
            $traces []= array( 'file'=>$sub[1], 'line'=>$sub[2],
                              'context' => $sub[3] );
    }
    
    echo "<table class='trace'><tr><th>File</th><th>Line</th><th>Context</th></tr>
    <tr class='odd'><td>$file</td><td class='linenum'>$line</td><td></td></tr>";
    foreach ( $traces as $c => $trace )
    {
        $class = $c % 2 ? 'odd' : 'even';
        echo "<tr class='$class'><td>{$trace['file']}</td>
            <td class='linenum'>{$trace['line']}</td>
            <td>{$trace['context']}</td></tr>";
    }
    echo "</table>";
}