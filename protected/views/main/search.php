<?php
/* @var $search Search */
/* @var $this MainController */
/* @var $form CustomForm */

echo '<div class="search">';

$form = $this->beginWidget('CustomForm', array(
    'action'=>array('main/search'),
    'method'=>'get',
	'id'=>'search-form',
    'model'=>$search,
    'auto_notes' => false,
));

echo $form->autoTextRow('term',array('style'=>'width: 100%;'));

echo $form->autoTextRow('username');

echo $form->autoCheckBoxRow('title_only');

echo $form->autoCheckBoxListRow('domains','PostType','id_post_type');

echo '<div class="row buttons">';
echo CHtml::submitButton('Search');
echo '</div>';
    
$this->endWidget();

echo '</div>';

if ( strlen(trim($search->term)) > 0 || strlen(trim($search->username)) > 0 ||
     !empty($search->domains))
{
    $provider = $search->search();

    $this->widget('DynamicPageGridView', array(
        'id'=>'search-grid',
        'cssFile'=>CHtml::normalizeUrl(array('main/css','sheet'=>'gridview')),
        'dataProvider' => $provider,
        'columns' => array(
            array(
                'name'=>'title',
                'type'=>'html',
                'value'=>'$data->link()'
            ),
            'type.name:text:Type',
            array(
                'header'=>'Author',
                'type'=>'html',
                'name'=>'user.name',
                'value'=>'$data->source->user->link()'
            ),
            'content.date'
        )
    ));
    
    $this->tracking_js = "piwikTracker.trackSiteSearch(".
            "'{$search->term}',false,{$provider->totalItemCount});";
}
