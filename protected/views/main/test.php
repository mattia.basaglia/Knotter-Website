<?php

if ( isset($_REQUEST['test-area']) )
    $text = $_REQUEST['test-area'];
else
    $text =
<<<HTML
<h3 class="title">Lorem ipsum dolor sit amet</h3>

<h4 class="title">consectetuer adipiscing elit.</h4>
<div class="toolbox gallery-stub" contenteditable="false" data-gallery="1">
    <h3 class="title">Stub</h3>
</div>
<p>Aenean <a href="http://example.com">commodo ligula</a> eget dolor. Ae<em>ne</em>an massa. <strong>Cum sociis natoque penatibus</strong> et magnis dis parturient montes, nascetur ridiculus mus. Donec quam<img alt="LOL" src="http://localhost/reknotter/img/icon/scalable/logo-big.svg" style="width: 278px; height: 278px;" /> felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>

<ul>
	<li>Nulla consequat massa quis enim.</li>
	<li>Donec pede justo,
	<ul>
		<li>fringilla vel,</li>
		<li>aliquet nec,</li>
		<li>vulputate eget,</li>
		<li>arcu.</li>
	</ul>
	</li>
	<li>In enim justo,
	<ul>
		<li>rhoncus ut,</li>
		<li>imperdiet a,</li>
		<li>venenatis vitae,</li>
		<li>justo.</li>
	</ul>
	</li>
</ul>

<blockquote>
<p>Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>

<p>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
</blockquote>

<p>Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.</p>

<p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.</p>

<pre>
Sed consequat, 
    leo eget bibendum sodales, augue velit cursus nunc, 
    quis gravida magna mi a libero. 
Fusce vulputate eleifend sapien. 
Vestibulum purus quam, 
    scelerisque ut,
    mollis sed, 
    nonummy id,
       metus. 
Nullam accumsan lorem in dui.       Cras ultricies mi eu turpis hendrerit fringilla. </pre>

<p>Vestibulum <b>ante ipsum <i>primis in</i> faucibus</b> orci <b>luctus</b> et
ultrices posuere cubilia <i>Curae;
In ac dui quis</i> mi consectetuer lacinia.</p>
HTML;

?>
<form method='post'>
<?php
    $this->widget('RichEditor',array(
        'name'=>"test-area",
        'id'=>"test-area",
        'value'=>$text
    ));
?>
    <input type='submit' id='submit' />
</form>

<div class='output'>
<h3>Filtered HTML</h3>
<div style='border:1px solid black' id='filtered'><?php

    $filtered_html = ConverterFacade::o()->admin_html_filter($text,$errors);
    
    if ( !empty($errors) )
    {
        echo "<pre>";
        foreach($errors as $err)
        {
            echo "{$err['line']}: ".CHtml::encode($err['what'])."\n";
        }
        echo "</pre>";
    }
    else
    {
        
        $processed_html = ConverterFacade::o()->html_render($filtered_html,$errors);
        
        echo $processed_html;
        
        if ( !empty($errors) )
        {
            echo "<pre>";
            foreach($errors as $err)
            {
                echo "{$err['line']}: ".CHtml::encode($err['what'])."\n";
            }
            echo "</pre>";
        }
    }

?></div>

<h3>Markdown</h3>
<div style='border:1px solid black' id='markdown'><?php

    $markdown = ConverterFacade::o()->html2md($text,$errors);
    echo "<pre>";
    echo $markdown;
    if ( !empty($errors) )
    {
        echo "\n\n\n\n";
        foreach($errors as $err)
        {
            echo "{$err['line']}: ".CHtml::encode($err['what'])."\n";
        }
    }
    echo "</pre>";

?></div>

<h3>HTML reverted from Markdown</h3>
<div style='border:1px solid black' id='revert'><?php

    $reverted_html = ConverterFacade::o()->md_render($markdown,$errors);
    
    if ( !empty($errors) )
    {
        echo "<pre>";
        foreach($errors as $err)
        {
            echo "{$err['line']}: ".CHtml::encode($err['what'])."\n";
        }
        echo "</pre>";
    }
    else
    {
        echo $reverted_html;
    }

?></div>

<h3>Back to editor</h3>
<?php
    $this->widget('RichEditor',array(
        'name'=>"test-area-round",
        'id'=>"test-area-round",
        'value'=>ConverterFacade::o()->md2html($markdown,$errors)
    ));
    if ( !empty($errors) )
    {
        echo "<pre>";
        foreach($errors as $err)
        {
            echo "{$err['line']}: ".CHtml::encode($err['what'])."\n";
        }
        echo "</pre>";
    }
?>
</div>

