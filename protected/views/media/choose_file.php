<?php

global $server_path;

function retrieve_files ( $base, $path )
{
    $fullpath = "$base/$path";
    if ( is_dir($fullpath) )
    {
        $children = scandir($fullpath);
        $output = array();
        foreach ( $children as $file )
        {
            if ( !preg_match('/^\.\.?$/',$file) )
            {
                $fname = "$fullpath/$file";
                $relname = ltrim("$path/$file","/");
                if ( is_file($fname) )
                    $output[]= $relname;
                else if ( is_dir($fname) )
                {
                    $output[$file] = retrieve_files($base,$relname);
                }
            }
        }
        ksort($output,SORT_STRING);
        return $output;
    }
    return null;
}

function print_files($thus,$dir,$name,$level)
{
    
    $items = array();
    foreach($dir as $file )
    {
        if ( ! is_array($file) )
        {
            $item = new MediaItem;
            $item->file = $file;
            $item->description =
                CHtml::link($file,array('newItem','file'=>$file));
            $items []= $item;
        }
    
    }
    
    $thus->beginWidget('GalleryRenderer',array(
        'cssClass'=>'toolbox image-preview',
        'title' => $name,
        'titleLevel'=>$level,
        'items'=>$items,
    ));
    
    
    foreach($dir as $name => $file )
    {
        if ( is_array($file) )
            print_files($thus,$file,$name,$level+1);
    
    }
    
    
    $thus->endWidget();
}

$dirs = retrieve_files("$server_path/img","");
print_files($this,$dirs,'img',2);

Yii::app()->clientScript->registerCss('small-images',
<<<CSS
.image-preview li img {
    max-width:256px;
    max-height:80px;
    vertical-align:middle;
    box-shadow: 2px 2px 5px gray;
}
CSS
);