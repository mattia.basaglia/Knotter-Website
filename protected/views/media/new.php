<?php
/**
 * @var $model MediaItem
 * @var $this MediaController
 * @var $form CustomForm
 */

$this->pageTitle = Yii::app()->name." | Media | $model->title " .
    ( $model->isNewRecord ? 'New' : 'Edit' );

if ( Yii::app()->user->has_role('admin') )
    $this->breadcrumbs ['Media']= array('media/index');
else
    $this->breadcrumbs []= 'Media';

$this->breadcrumbs [$model->title]= array('view','id'=>$model->id_media_item);


$form = $this->beginWidget('CustomForm',array(
    'model'=>$model
));

echo $form->autoTextRow('title');
echo $form->autoTextAreaRow('description');

echo '<div class="main-form">';
echo $form->autoTextRow('file');
echo $form->autoTextRow('thumb');
echo $form->autoTextRow('mime_type');
echo '</div>';

echo '<div class="options-form" >';
echo $model->render(array('id'=>'preview'));
echo '</div>';

echo $form->submitRow('Save');

$this->endWidget();


$imgpath = Controller::base_url()."/img/";
Yii::app()->clientScript->registerScript('update-preview',
<<<JS
$('#MediaItem_file').focusout( function(){
    $('#preview').attr('src','$imgpath'+$(this).val());
});
JS
);