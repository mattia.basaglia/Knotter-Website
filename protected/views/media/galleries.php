<?php
$gallery = Gallery::model()->findAll();
foreach ( $gallery as $g )
    $this->widget('GalleryRenderer',array(
        'gallery' => $g,
        'item_height'=>128,
        'collapsed' => true,
        'linkUrl' => array('gallery','id'=>$g->id_gallery),
    ));