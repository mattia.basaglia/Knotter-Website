<?php

$this->breadcrumbs = array('Media');


$this->beginWidget('Toolbox',array(
    'collapsed' => true,
    'titleLevel'=>4,
    'title'=>'All Items',
));
$items = MediaItem::model()->findAll();
Yii::app()->clientScript->registerCss($this->id,
    ".media-gallery img { height:128px; }" );
echo "<ul class='media-gallery'>";
foreach ( $items as $item )
    echo "<li>".CHtml::link($item->render_thumb(),$item->desc_url()).'</li>';
echo '</ul>';
$this->endWidget();
    
$gallery = Gallery::model()->findAll();
foreach ( $gallery as $g )
    $this->widget('GalleryRenderer',array(
        'gallery' => $g,
        'item_height'=>128,
        'collapsed' => true,
        'linkUrl' => array('gallery','id'=>$g->id_gallery),
    ));
    
