<?php

$this->pageTitle = $this->pageTitle." | ".$gallery->name;

    
$this->breadcrumbs ['Gallery'] = array('media/gallery');
$this->breadcrumbs []= $gallery->name;

$this->link_actions []= array('label'=>'Delete', 'url'=>'#',
    'linkOptions'=>array('submit'=>array('deleteGallery',
                            'id'=>$gallery->id_gallery),
    'confirm'=>'Are you sure you want to delete this Gallery?'));

$title = $gallery->name;
$this->widget('GalleryRenderer',array(
                'gallery' => $gallery,
                'item_height'=>128,
                'id'=>'current-gallery',
            ));

if ( Yii::app()->user->has_role('admin') )
{
    
    $tb = $this->beginWidget('Toolbox',array(
        'title'=>'Manage Gallery',
        'titleLevel' => 4,
    ));
    
    $form = $this->beginWidget('CustomForm',array(
        'auto_notes' => false,
        'method'=>'get'
    ));
    echo CHtml::label('Title','Gallery[name]',array('class'=>'inline')). ' ';
    echo $form->textField($gallery,'name').' ';
    echo CHtml::submitButton('Update',array('id'=>'update-title'));
    $this->endWidget();
    
    
    $update_js = "jQuery('#item-editor').yiiGridView('update');".
        "jQuery('#current-gallery_content').load('".
        CHtml::normalizeUrl(array('gallery','id'=>$_GET['id'])).
        " #current-gallery_items');";
    
function button($label,$img,$url,$update_js)
{
    return array(
        'label' => $label,
            'url'=>'array("media/'.$url.'","gallery"=>$data->id_gallery,
                                "item"=>$data->id_media_item)',
            'imageUrl'=>Controller::base_url()."/img/icon/16x16/$img.png",
            'click'=>
<<<JS
function() {
var th = this;
jQuery('#item-editor').yiiGridView('update', {
    type: 'POST',
    url: jQuery(this).attr('href'),
    success: function(data) {
        $update_js
    },
});
return false;
}
JS
    );
}
        
$this->widget('zii.widgets.grid.CGridView',array(
    'dataProvider'=>new CArrayDataProvider($gallery->item_positions,array(
                            'keyField'=>false,
                            'pagination'=>false,
                            'sort'=>array('defaultOrder'=>'order'),
                    )),
    'enablePagination' => false,
    'cssFile'=>CHtml::normalizeUrl(array('main/css','sheet'=>'gridview')),
    'id' => 'item-editor',
    'columns' => array(
        array(
            'class'=>'CDataColumn',
            'header'=>'Item',
            'type'=>'html',
            'value'=>'CHtml::link($data->item->id_string(),
                        $data->item->desc_url())',
        ),
        'order',
        array( 'class'=>'CButtonColumn',
            'template'=>'{move-up} {move-down} {delete}',
            'buttons'=>array(
            'move-up'=> button('Move up','go-up','move_up',$update_js),
            'move-down'=> button('Move Down','go-down','move_down',$update_js),
            'delete'=> button('Delete','edit-delete','unlink',$update_js),
        ))
    )
));

    
    $form = $this->beginWidget('CustomForm',array(
        'action' => array('link','gallery'=>$gallery->id_gallery),
        'auto_notes' => false,
        'method'=>'get',
        'id'=>'add-form',
    ));
    echo CHtml::label('Item','item',array('class'=>'inline')). ' ';
    echo $form->plainAutoComplete('item',array('item_autocomplete')). ' ';
    echo CHtml::button('Add',array(
            'src'=>Controller::base_url()."/img/icon/22x22/list-add.png",
            'type'=>'image',
            'id'=>'add-button'
        ));
    $this->endWidget(); // form
    Yii::app()->clientScript->registerScript('dynamic-update',
        "$('#add-button').click(function(){
            $.ajax({
                type: 'GET',
                url: $('#add-form').attr('action')+'&item='+$('#item').val(),
                success: function(data) {
                    $update_js
                    $('#item').val('');
                    $('#item_autocomplete').val('');
                },
            });
            return false;
        });
        
        $('#update-title').click(function(){
            $.ajax({
                type: 'GET',
                url: '?Gallery[name]='+encodeURIComponent($('#Gallery_name').val())+'&ajax=1',
                success: function(data) {
                    var content = $(data);
                    var title = $('#current-gallery_title');
                    title.text(
                        $('#current-gallery_title',content).text()
                    );
                    title.animate({opacity:'0.5',color:'red'},200);
                    title.animate({opacity:'1',color:'inherit'},1000);
                },
            });
            return false;
        });
        "
    );
    
    $this->endWidget(); // toolbox
    
    
}