<?php
/* @var $item MediaItem */

$this->pageTitle = Yii::app()->name." | Media | $item->title";

if ( Yii::app()->user->has_role('admin') )
    $this->breadcrumbs ['Media']= array('media/index');
else
    $this->breadcrumbs []= 'Media';

$this->breadcrumbs []= $item->title;

$this->link_actions []= array('label'=>'Update',
                        'url'=>array('editItem','id'=>$item->id_media_item) );
$this->link_actions []= array('label'=>'Delete', 'url'=>'#',
    'linkOptions'=>array('submit'=>array('deleteItem','id'=>$item->id_media_item),
    'confirm'=>'Are you sure you want to delete this item?'));

echo "<h2 class='title'>$item->title</h2>";
echo $item->render_link();
echo "<div class='description'>$item->description</div>";

if ( Yii::app()->user->has_role('admin') )
{
    $tb = $this->beginWidget('Toolbox',array(
        'title'=>'Galleries',
        'titleLevel' => 4,
        'collapsed'=>true,
    ));
    
    $this->widget('zii.widgets.grid.CGridView',array(
        'dataProvider'=>new CArrayDataProvider($item->gallery_positions,array(
                                'keyField'=>false,
                                'pagination'=>false,
                        )),
        'enablePagination' => false,
        'cssFile'=>CHtml::normalizeUrl(array('main/css','sheet'=>'gridview')),
        'columns' => array(
            array(
                'class'=>'CDataColumn',
                'header'=>'Gallery',
                'type'=>'html',
                'value'=>'CHtml::link($data->gallery->name,
                            array("media/gallery","id"=>$data->id_gallery))',
            ),
            'order'
        )
    ));
    
    $this->endWidget();
}