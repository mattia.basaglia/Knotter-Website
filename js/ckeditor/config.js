/**
 * @license Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */


CKEDITOR.stylesSet.add( 'knotter', [
    //{ name: 'Title', element: 'h2', attributes: { 'class': 'title' } },
    { name: 'SubTitle', element: 'h3', attributes: { 'class': 'title' } },
    { name: 'SubSubTitle', element: 'h4', attributes: { 'class': 'title' } },
    { name: 'ParagraphTitle', element: 'h5', attributes: { 'class': 'title' } },
    { name: 'SubParagraphTitle', element: 'h6', attributes: { 'class': 'title' } },
    { name: 'Paragraph', element: 'p' },
    { name: 'Message', element: 'div', attributes: { 'class': 'message' } },
    { name: 'Error', element: 'div', attributes: { 'class': 'error' } },
    { name: 'Success', element: 'div', attributes: { 'class': 'success' } },
    { name: 'Note', element: 'div', attributes: { 'class': 'note' } },
]);

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	config.toolbar = [
    { name: 'clipboard', groups: [ 'clipboard', 'undo' ],
        items: [ 'Cut', 'Copy', 'Paste', '-', 'Undo', 'Redo' ] },
        
    { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
    { name: 'insert', items: [ 'Image','GalleryButton', '-', 'Table', 'SpecialChar' ] },
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ],  items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
    { name: 'paragraph', groups: [ 'list' ], items: [ 'NumberedList', 'BulletedList', 'Blockquote', '-', 'Outdent', 'Indent', ] },
    { name: 'styles', items: [ 'Styles' ] },
    { name: 'about', items: [ 'About' ] },
    { name: 'tools', items: [ 'Maximize' ] },
    { name: 'document', groups: [ 'mode', 'document' ], items: [ 'Source' ] },
    ];
    config.stylesSet= 'knotter';
    config.disableNativeSpellChecker = false;


};  


CKEDITOR.dialog.add( 'GalleryDialog', function( editor ) { return {
    title : 'Insert Gallery',
    minWidth : 240,
    minHeight : 80,
    contents : [{
        id : 'main-tab',
        label : 'Gallery',
        title : 'Gallery',
        elements :
        [
            {
                id : 'id-gallery',
                type : 'select',
                label : 'Select Gallery',
                items: [],
            }
        ]
    }],
    onOk : function(){
        
        var element = CKEDITOR.dom.element.createFromHtml(
            '<div class="toolbox gallery-stub" contenteditable="false" data-gallery="'+
                this.getValueOf('main-tab','id-gallery') +'"><h3 class="title">'+
                $('#'+this.getContentElement('main-tab','id-gallery').domId
                        +' option:selected').text()
            +'</h3></div>');
        editor.insertElement(element);
    },
};});